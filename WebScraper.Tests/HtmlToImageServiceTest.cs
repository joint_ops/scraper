﻿using CoreHtmlToImage;
using Shouldly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using WebScraper.Core.Services;
using WebScraper.Infra.Services;
using Xunit;

namespace WebScraper.Tests
{
    public class HtmlToImageServiceTest
    {
      
        [Fact]
        public  void Create_ShouldCreateTable()
        {
            var converter = new HtmlConverter();
            var html = File.ReadAllText("table.html");
            var bytes = converter.FromHtmlString(html);
            File.WriteAllBytes("table.jpg", bytes);

            File.Exists("table.jpg").ShouldBeTrue();

        }
    }
}
