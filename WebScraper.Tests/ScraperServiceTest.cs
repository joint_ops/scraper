﻿using HtmlAgilityPack;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using WebScraper.Core.Services;
using WebScraper.Infra.Services;
using Xunit;

namespace WebScraper.Tests
{
    public class ScraperServiceTest
    {
        private readonly IScraperService _scraperService = new ScraperService();       
        [Fact]
        public void GetCompanyName_ShouldReturnCompanyName()
        {
            //Arrange
            var path = @"Natrol.html";

            //Act
            var name =_scraperService.GetCompanyName(path);

            name.ShouldBe("Natrol");
        }
       
        [Fact]
        public void GetProductsUrls_ShouldReturnProductsUrlList()
        {
            //Arrange
            var path = @"Natrol.html";

            //Act
            var list = _scraperService.GetProductsUrls(path);

            //Assert
            list.Count.ShouldBe(24);
            list.FirstOrDefault().ShouldBe("https://pl.iherb.com/pr/Natrol-Biotin-Maximum-Strength-10-000-mcg-100-Tablets/25842");
            list.Last().ShouldBe("https://pl.iherb.com/pr/Natrol-Alpha-Lipoic-Acid-600-mg-30-Capsules/6913");
        }
        [Fact]
        public void GetProductsNames_ShouldReturnProductsNamesList()
        {
            //Arrange
            var path = @"Natrol.html";

            //Act
            var list = _scraperService.GetProductsNames(path);

            list.Count.ShouldBe(24);
            list.FirstOrDefault().ShouldBe("Natrol, Biotin, Maximum Strength, 10,000 mcg, 100 Tablets");
            list.Last().ShouldBe("Natrol, Alpha Lipoic Acid, 600 mg, 30 Capsules");

        }
        [Fact]
        public void GetPrice_ShouldReturnPrice()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var price = _scraperService.GetPrice(path);

            price.ShouldBe(37, 42);
        }
        [Fact]
        public void GetCurrency_ShouldReturnCurrency()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var currency = _scraperService.GetCurrency(path);

            //Assert
            currency.ShouldBe("PLN");
        }
        [Fact]
        public void GetImages_ShouldReturnImagesList()
        {
            //Arrange
            var path = @"melatonin.html";

            //Act
            var images = _scraperService.GetImages(path);

            //Assert
            images.Count.ShouldBe(3);
            images.FirstOrDefault().ShouldBe("https://s3.images-iherb.com/ntl/ntl05964/l/33.jpg");
            images.ElementAt(1).ShouldBe("https://s3.images-iherb.com/ntl/ntl05964/l/34.jpg");
            images.Last().ShouldBe("https://s3.images-iherb.com/ntl/ntl05964/l/35.jpg");
        }
        /*
        [Fact]
        public void GetStockStatus_ShouldReturnStockStatus()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var stockMeta = _scraperService.GetStockStatus(path);

            //Assert
            stockMeta.ShouldBe(true);
        }
        */
        [Fact]
        public void GetProductCode_ShouldReturnProductCode()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var productCode = _scraperService.GetProductCode(path);

            //Assert
            productCode.ShouldBe("NTL-05396");
        }
        [Fact]
        public void GetUpcCode_ShouldReturnUpcCode()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var upcCode = _scraperService.GetUpcCode(path);

            //Assert
            upcCode.ShouldBe("047469053963");
        }
        [Fact]
        public void GetQuantity_ShouldReturnQuantity()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            string quantity = _scraperService.GetQuantity(path);

            //Assert
            quantity.ShouldBe("100 Count");
        }
        [Fact]
        public void GetTable_ShouldReturnHtmlTable()
        {  
            //Arrange
            var path = @"Product.html";

            //Act
            var table = _scraperService.GetSuplementTable(path);

            //Assert
            table.ShouldNotBeNullOrWhiteSpace();
        }
        [Fact]
        public void GetDescription_ShouldReturnDescription()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var description = _scraperService.GetDescription(path);

            //Assert
            description.ShouldNotBeNullOrWhiteSpace();
            description.ShouldBe("");
        }
        [Fact]
        public void GetExpirationDate_ShouldReturnExpirationDate()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var description = _scraperService.GetExpirationDate(path);

            //Assert
            description.Value.Year.ShouldBe(2020);
            description.Value.Month.ShouldBe(11);
        }
        [Fact]
        public void GetDimensions_ShouldReturnDimensions()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var description = _scraperService.GetDimensions(path);

            //Assert
            description.ShouldBe("10.7 x 4.8 x 4.8 cm");
        }
        [Fact]
        public void GetShippingWeight_ShouldReturnShippingWeight()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var description = _scraperService.GetShippingWeight(path);

            //Assert
            description.ShouldBe("0.07 kg");
        }
        [Fact]    
        public void GetUrl_ShouldReturnUrl()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var description = _scraperService.GetUrl(path);

            //Assert
            description.ShouldBe("https://pl.iherb.com/pr/Natrol-Biotin-Maximum-Strength-10-000-mcg-100-Tablets/25842");
        }
       
        [Fact]
        public void GetCategory_ShouldReturnCategory()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var description = _scraperService.Get1stCategory(path);

            //Assert
            description.ShouldBe("Biotin");
        }
        [Fact]
        public void GetDescriptionList_ShouldReturnDescriptionList()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var description = _scraperService.GetDescriptionList(path);

            //Assert
            description.ShouldBe("Biotin");
        }
        [Fact]
        public void GetSuggestedUse_ShouldReturnSuggestedUse()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var description = _scraperService.GetSuggestedUse(path);

            //Assert
            description.ShouldBe("Biotin");
        }
        [Fact]
        public void GetIngredients_ShouldReturnIngredients()
        {
            //Arrange
            var path = @"Product.html";

            //Act
            var description = _scraperService.GetIngredients(path);

            //Assert
            description.ShouldBe("Biotin");
        }
       

    }
}
