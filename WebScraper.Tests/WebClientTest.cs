﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WebScraper.Core.Services;
using WebScraper.Infra.Services;
using Xunit;

namespace WebScraper.Tests
{
    public class WebClientTest
    {
        private readonly IScraperService _scraperService = new ScraperService();
        WebClient webClient = new WebClient();
        [Fact]
        public async Task ShouldSuccedd()
        {
            string output = null;
            string url = "https://pl.iherb.com/pr/acure-brightening-facial-scrub-4-fl-oz-118-ml/36379";
            using (webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;

                try
                {
                    output = await webClient.DownloadStringTaskAsync(url);
                }
                catch (Exception ex)
                {
                   throw ex;
                }
            }
            output.ShouldNotBeNull();
            var currency = _scraperService.GetCurrency(output);

            //Assert
            currency.ShouldBe("PLN");
        }
        [Fact]
        public async Task ShouldGetCurrencyInPLN()
        {
            string output = null;
            string url = "https://pl.iherb.com/pr/acure-brightening-facial-scrub-4-fl-oz-118-ml/36379";
            using (webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;

                try
                {
                    output = await webClient.DownloadStringTaskAsync(url);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            var currency = _scraperService.GetCurrency(output);

            //Assert
            currency.ShouldBe("PLN");
        }
        [Fact]
        public async Task ShouldSwitchCurrencyToUSDAfterAddingCookies()
        {
            string output = null;
            string url = "https://pl.iherb.com/pr/acure-brightening-facial-scrub-4-fl-oz-118-ml/36379";
            using (webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;
                webClient.Headers.Add(HttpRequestHeader.Cookie, "ih-preference=store=0&country=PL&language=de-DE&currency=USD;" + "iher-pref1=storeid=0&sccode=PL&lan=de-DE&scurcode=USD&wp=1&lchg=1&ifv=1&bi=0");

                try
                {
                    output = await webClient.DownloadStringTaskAsync(url);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            var currency = _scraperService.GetCurrency(output);

            //Assert
            currency.ShouldBe("USD");
        }
        [Fact]
        public async Task ShouldSwitchCurrencyToEURAfterAddingCookies()
        {
            string output = null;
            string url = "https://pl.iherb.com/pr/acure-brightening-facial-scrub-4-fl-oz-118-ml/36379";
            using (webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;
                webClient.Headers.Add(HttpRequestHeader.Cookie, "ih-preference=store=0&country=PL&language=de-DE&currency=EUR;" + "iher-pref1=storeid=0&sccode=PL&lan=de-DE&scurcode=EUR&wp=1&lchg=1&ifv=1&bi=0");

                try
                {
                    output = await webClient.DownloadStringTaskAsync(url);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            var currency = _scraperService.GetCurrency(output);

            //Assert
            currency.ShouldBe("EUR");
        }
        [Fact]
        public async Task ShouldGetDescriptionInGerman()
        {
            string output = null;
            string url = "https://pl.iherb.com/pr/acure-brightening-facial-scrub-4-fl-oz-118-ml/36379";
            using (webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;
                webClient.Headers.Add(HttpRequestHeader.Cookie, "ih-preference=store=0&country=PL&language=de-DE&currency=EUR;" + "iher-pref1=storeid=0&sccode=PL&lan=de-DE&scurcode=EUR&wp=1&lchg=1&ifv=1&bi=0");

                try
                {
                    output = await webClient.DownloadStringTaskAsync(url);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            var description = _scraperService.GetDescription(output);

            //Assert
            description.ShouldBe("EUR");
        }
        [Fact]
        public async Task ShouldGetNameWithCorrectGermanDiacritics()
        {
            string output = null;
            string url = "https://pl.iherb.com/pr/acure-the-essentials-castor-oil-1-fl-oz-30-ml/96798";
            using (webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;
                webClient.Headers.Add(HttpRequestHeader.UserAgent, "");
                webClient.Headers.Add(HttpRequestHeader.Cookie, "ih-preference=store=0&country=PL&language=de-DE&currency=EUR;" + "iher-pref1=storeid=0&sccode=PL&lan=de-DE&scurcode=EUR&wp=1&lchg=1&ifv=1&bi=0");

                try
                {
                    output = await webClient.DownloadStringTaskAsync(url);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            var description = _scraperService.GetSingleProductName(output);

            //Assert
            description.ShouldBe("EUR");
        }
    }
}
