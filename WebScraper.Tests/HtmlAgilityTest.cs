using HtmlAgilityPack;
using Shouldly;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Xunit;

namespace WebScraper.Tests
{
    public class HtmlAgilityTest
    {
        [Fact]
        public void GetCompanyName_ShouldReturnCompanyName()
        {
            var path = @"Natrol.html";
            var list = new List<string>() { };

            var doc = new HtmlDocument();
            doc.Load(path);
        
            var name = doc.DocumentNode.SelectSingleNode("//h1[contains(@class, 'sub-header-title')]").InnerText.Trim();
          
            name.ShouldBe("Natrol");           
        }
        [Fact]
        public void GetProductsUrls_ShouldReturnProductsUrlList()
        {
            var path = @"Natrol.html";
            var list = new List<string>() { };

            var doc = new HtmlDocument();
            doc.Load(path);

            var products = doc.DocumentNode.SelectNodes("//div[contains(@class, 'product-inner')]");

            foreach (var product in products)
            {
                string url = product.SelectSingleNode("//a[contains(@class, 'product-link')]").GetAttributeValue("href", "product link not found");
                list.Add(url);
            }

            list.Count.ShouldBe(24);
            list.FirstOrDefault().ShouldBe("https://pl.iherb.com/pr/Natrol-Biotin-Maximum-Strength-10-000-mcg-100-Tablets/25842");

        }
        [Fact]
        public void GetPrice_ShouldReturnPrice()
        {
            var path = @"Product.html";
            var doc = new HtmlDocument();
            doc.Load(path);

            var priceMeta = doc.DocumentNode.SelectSingleNode("//meta[@property='og:price:amount']").GetAttributeValue("content", "price meta content not found");
            var price = decimal.Parse(priceMeta, CultureInfo.InvariantCulture);

            price.ShouldBe(37,42);
        }
        [Fact]
        public void GetImages_ShouldReturnImagesList()
        {
            var path = @"Product.html";
            var list = new List<string>() { };

            var doc = new HtmlDocument();
            doc.Load(path);

            var images = doc.DocumentNode.SelectNodes("//img[contains(@class, 'lazy')]");

            foreach (var image in images)
            {
               var src = image.GetAttributeValue("data-lazyload", "price meta content not found");
               list.Add(src);
            }

            images.Count.ShouldBe(2);
            list.FirstOrDefault().ShouldBe("https://s3.images-iherb.com/ntl/ntl05396/y/11.jpg");
            list.Last().ShouldBe("https://s3.images-iherb.com/ntl/ntl05396/y/12.jpg");
        }
        [Fact]
        public void GetStockStatus_ShouldReturnStockStatus()
        {
            var path = @"Product.html";
            var doc = new HtmlDocument();
            doc.Load(path);

            var stockMeta = doc.DocumentNode.SelectSingleNode("//meta[@property='og:availability']").GetAttributeValue("content", "stock status meta content not found");
           
            stockMeta.ShouldBe("instock");
        }
        [Fact]
        public void GetProductCode_ShouldReturnProductCode()
        {
            var path = @"Product.html";
            var doc = new HtmlDocument();
            doc.Load(path);

            var productCode = doc.GetElementbyId("product-specs-list").Elements("li").ElementAt(2).Element("span").InnerText;

            productCode.ShouldBe("NTL-05396");
        }
        [Fact]
        public void GetUpcCode_ShouldReturnUpcCode()
        {
            var path = @"Product.html";
            var doc = new HtmlDocument();
            doc.Load(path);

            var upcCode = doc.GetElementbyId("product-specs-list").Elements("li").ElementAt(3).Element("span").InnerText;

            upcCode.ShouldBe("047469053963");
        }
        [Fact]
        public void GetQuantity_ShouldReturnQuantity()
        {
            var path = @"Product.html";
            var doc = new HtmlDocument();
            doc.Load(path);

            var text = doc.GetElementbyId("product-specs-list").Elements("li").ElementAt(4).InnerText;
            string quantity = text.Replace("Package Quantity:","").Trim();

            quantity.ShouldBe("100 Count");
        }
        [Fact]
        public void GetTable_ShouldReturnHtmlTable()
        {
            var path = @"Product.html";
            var doc = new HtmlDocument();
            doc.Load(path);

            var table = doc.DocumentNode.SelectSingleNode("//div[contains(@class, 'supplement-facts-container')]").InnerHtml.Trim();

            table.ShouldNotBeNullOrWhiteSpace();
        }
        [Fact]
        public void GetDescription_ShouldReturnDescription()
        {
            var path = @"Product.html";
            var doc = new HtmlDocument();
            doc.Load(path);

            var text = doc.DocumentNode.SelectSingleNode("//div[contains(@class, 'inner-content')]").Element("div").Element("div").Element("div").Element("div").Element("div").Element("div").Element("ul").InnerText.Trim();
            var description = Regex.Replace(text, @"\s+", " ").Replace("&amp;","");

            description.ShouldNotBeNullOrWhiteSpace();
        }
    }
}
