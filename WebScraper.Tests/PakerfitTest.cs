﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WebScraper.Core.Services;
using WebScraper.Infra.Services;
using Xunit;

namespace WebScraper.Tests
{
    public class PakerfitTest
    {

        //Login: biuro@provitax.pl
        //  Hasło: Provitax321!

        private readonly IScraperService _scraperService = new ScraperService();
        CookieAwareWebClient webClient = new CookieAwareWebClient();
        [Fact]
        public async Task ShouldSuccedd()
        {
            string output = null;
            string url = "https://pl.iherb.com/pr/acure-brightening-facial-scrub-4-fl-oz-118-ml/36379";
            using (webClient = new CookieAwareWebClient())
            {
                webClient.Encoding = Encoding.UTF8;

                try
                {
                    output = await webClient.DownloadStringTaskAsync(url);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            output.ShouldNotBeNull();
            var currency = _scraperService.GetCurrency(output);

            //Assert
            currency.ShouldBe("PLN");
        }
        [Fact]
        public async Task ShouldSwitchCurrencyToUSDAfterAddingCookies()
        {
            string output = null;
            string url = "https://www.paker.fit/scitec/100-casein-complex.html";
            using (webClient = new CookieAwareWebClient())
            {
                webClient.Encoding = Encoding.UTF8;            
              //  webClient.UseDefaultCredentials = true;
                webClient.Headers.Add(HttpRequestHeader.Cookie, "frontend_cid=lZs9YTzVmWOW7uZr;" + "pkuoqbp1a70p6aieaal2gb9t9h");
                var loginData = new NameValueCollection();
                loginData.Add("login[username]", "biuro@provitax.pl");
                loginData.Add("login[password]", "Provitax321!");
               // loginData.Add("username", "biuro@provitax.pl");
                //loginData.Add("password", "Provitax321!");
                loginData.Add("form_key", "ZD2mtp0hTwT1rus8");
                webClient.UploadValues("https://www.paker.fit/customer/account/loginPost/", "POST", loginData);


                try
                {
                    output = await webClient.DownloadStringTaskAsync(url);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
           // var currency = _scraperService.GetCurrency(output);

            //Assert
            output.ShouldNotBeNull();
            output.ShouldBe("x");
            //currency.ShouldBe("USD");
        }


        public class CookieAwareWebClient : WebClient
        {
            private CookieContainer cookie = new CookieContainer();

            protected override WebRequest GetWebRequest(Uri address)
            {
                WebRequest request = base.GetWebRequest(address);
                if (request is HttpWebRequest)
                {
                    (request as HttpWebRequest).CookieContainer = cookie;
                }
                return request;
            }
        }
    }
}
