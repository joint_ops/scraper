﻿using CoreHtmlToImage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using WebScraper.Core.Services;

namespace WebScraper.Infra.Services
{
    public class HtmlToImageService : IHtmlToImageService
    {
        public async void CreateImage(string html, string path)
        {
            var converter = new HtmlConverter();
            var bytes = converter.FromHtmlString(html);
            await File.WriteAllBytesAsync(path, bytes);
        }
    }
}
