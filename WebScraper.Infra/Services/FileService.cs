﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Core.Services;

namespace WebScraper.Infra.Services
{
    public class FileService : IFileService
    {   
        public async Task<string> DownloadFileAsync(string url, string fileName,[Optional]CancellationToken token)
        {

            using (WebClient webClient = new WebClient())
            {
                Uri URL = url.StartsWith("https://", StringComparison.OrdinalIgnoreCase) ? new Uri(url) : new Uri("https://" + url);

                try
                {
                    await webClient.DownloadFileTaskAsync(URL,fileName);
                    WaitForFile(fileName,token);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return url;
        }      
        public string ReadFile(string textFile)
        {
            if (!File.Exists(textFile))
                throw new Exception("file not found.");

            return File.ReadAllText(textFile);
        }
        private static bool IsFileReady(string filename)
        {
            // If the file can be opened for exclusive access it means that the file
            // is no longer locked by another process.
            try
            {
                using (FileStream inputStream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                    return inputStream.Length > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private static void  WaitForFile(string filename, CancellationToken token)
        {
            while (!IsFileReady(filename)) 
            {
                if (token.IsCancellationRequested)
                {
                    return;
                }
            }
        }

    }

   

   
}
