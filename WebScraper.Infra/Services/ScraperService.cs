﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using WebScraper.Core.Services;

namespace WebScraper.Infra.Services
{
    public class ScraperService : IScraperService
    {
       

        public string GetCompanyName(string html)
        {      
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            string companyName = doc.DocumentNode.SelectSingleNode("//h1[contains(@class, 'sub-header-title')]")?.InnerHtml.Trim() ?? null;
            return FixEncoding(companyName);
        }
        public List<string> GetProductsUrls(string html)
        {           
            var list = new List<string>();
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var products = doc.DocumentNode.SelectNodes("//a[contains(@class, 'product-link')]");

            for (int i = 0; i < products.Count; i++)
            {
                var url = products[i].GetAttributeValue("href", null);
                list.Add(url);
            }
            
            return list;        
        }
        public List<string> GetProductsNames(string html)
        {          
            var list = new List<string>();
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var products = doc.DocumentNode.SelectNodes("//div[contains(@class, 'product-title')]");

            foreach (var product in products)
            {
                string text = product?.InnerText.Trim() ?? null;
                var name = FixEncoding(text);
                list.Add(name);
            }
            return list;
        }
        public string GetSingleProductName(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var name = doc.DocumentNode.SelectSingleNode("//meta[@property='og:title']").GetAttributeValue("content", null);

            return FixEncoding(name);
        }
        public string GetBrand(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var name = doc.DocumentNode.SelectSingleNode("//meta[@property='og:brand']").GetAttributeValue("content", null);

            return FixEncoding(name);
        }
        public decimal GetPrice(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var priceMeta = doc.DocumentNode.SelectSingleNode("//meta[@property='og:price:amount']").GetAttributeValue("content", null);
            var price = decimal.Parse(priceMeta, CultureInfo.InvariantCulture);

            return price;
        }
        public string GetCurrency(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var currency = doc.DocumentNode.SelectSingleNode("//meta[@property='og:price:currency']").GetAttributeValue("content", null);

            return currency;
        }
        public List<string> GetImages(string html)
        {
            var list = new List<string>();
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var images = doc.DocumentNode.SelectNodes("//div[contains(@class, 'thumbnail-container')]/img");

            if (images != null)
            {
                foreach (var image in images)
                {
                    var src = image.GetAttributeValue("data-large-img", null);
                    list.Add(src);
                }
                return list;              
            }
            else
            {
                string src = doc.GetElementbyId("iherb-product-image")?.GetAttributeValue("src", null);

                if (src != null)
                {
                    list.Add(src);
                }            
            }

            return list;
        }       
        public string GetStockStatus(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            var outOfStockStatus = doc.DocumentNode.SelectSingleNode("//div[contains(@class, 'stock-status-text')]")?.InnerText ?? null;
            if (!string.IsNullOrEmpty(outOfStockStatus))
            {
                return FixEncoding(outOfStockStatus);
            }
            var inStockStatus = doc.GetElementbyId("stock-status").InnerText ?? null;
            return FixEncoding(inStockStatus);
        }
        public DateTime? GetExpirationDate(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            string text = null;
            string expirationDate = null;
            string modalText = doc.DocumentNode.SelectSingleNode("//div[contains(@class, 'modal-expiration-message-link')]")?.InnerText ?? null;

            if (doc.GetElementbyId("product-specs-list").Elements("li").ElementAt(0).InnerText.Contains("Expiration Date:"))
            {
                text = doc.GetElementbyId("product-specs-list").Elements("li").ElementAt(0).InnerText ?? null;
            }
            if (text != null)
            {
                if (modalText != null)
                {
                    expirationDate = text.Replace(modalText, "").Replace("Expiration Date:", "").Replace("?", "").Trim();
                }
            }
           
           
            return GetDateFromString(expirationDate);
        }
        public string GetShippingWeight(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            string weight = doc.GetElementbyId("actual-weight").InnerText ?? null;
            return weight.Trim();
        }
        public string GetDimensions(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            string dimensions  = doc.GetElementbyId("dimensions").InnerText ?? null;
            dimensions.Replace("Dimensions:", "");
            dimensions.Replace("Switch to Imperial units", "");
            return dimensions.Trim();
        }
        public string GetProductCode(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            string productCode;
            if (doc.GetElementbyId("expiration-message-link") == null)
            {
                productCode = doc.GetElementbyId("product-specs-list").Elements("li").ElementAt(2).Element("span")?.InnerText ?? null;
            }
            else
            {
                productCode = doc.GetElementbyId("product-specs-list").Elements("li").ElementAt(3).Element("span")?.InnerText ?? null;
            }

            return productCode;
        }
        public string GetUpcCode(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            string upcCode;
            if (doc.GetElementbyId("expiration-message-link") == null)
            {
                upcCode = doc.GetElementbyId("product-specs-list").Elements("li").ElementAt(3).Element("span")?.InnerText ?? null;
            }
            else
            {
                upcCode = doc.GetElementbyId("product-specs-list").Elements("li").ElementAt(4).Element("span")?.InnerText ?? null;
            }

            return upcCode;           
        }
        public string GetQuantity(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            string text = null;
            string quantity = null;
            if (doc.GetElementbyId("expiration-message-link") == null)
            {
                text = doc.GetElementbyId("product-specs-list").Elements("li").ElementAt(4)?.InnerText ?? null;
            }
            else
            {
                text = doc.GetElementbyId("product-specs-list").Elements("li").ElementAt(5)?.InnerText ?? null;
            }

            if (text.Contains("Package Quantity:"))
            {
                quantity = text.Replace("Package Quantity:", "").Trim();
            }
                     
            return quantity;
        }      
        public string GetDescription(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var text = doc.DocumentNode.SelectSingleNode("//div[contains(@class, 'inner-content')]").Element("div").Element("div").InnerText.Trim() ?? null;
            var description = Regex.Replace(text, @"\s+", " ").Replace("Description","");

            return FixEncoding(description);
        }
       
        public string GetDescriptionList(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var text = doc.DocumentNode.SelectSingleNode("//div[contains(@class, 'inner-content')]//div[contains(@class, 'row item-row')]")?.InnerText ?? null;
            var description = Regex.Replace(text, @"\s+", " ").Replace("Description", "");

            return FixEncoding(description);
        }
        public string GetSuggestedUse(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var text = doc.DocumentNode.SelectSingleNode("//div[contains(@class, 'prodOverviewDetail')]")?.InnerText.Trim() ?? null;

            return FixEncoding(text);
        }
        public string GetIngredients(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var text = doc.DocumentNode.SelectSingleNode("//div[contains(@class, 'prodOverviewIngred')]")?.InnerText.Trim() ?? null;

            return FixEncoding(text);
        }
        public string GetSuplementTable(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var table = doc.DocumentNode.SelectSingleNode("//div[contains(@class, 'supplement-facts-container')]")?.InnerHtml.Trim() ?? null;
            return table;
            
        }
        private static string FixEncoding(string text)
        {
            string replaced = null;

            if (!string.IsNullOrEmpty(text))
            {
               replaced = text.Replace("&amp;", "&").Replace("& x27;", "'").Replace("& x2B;", "+").Replace("&nbsp", " ").Replace("&#x27;", "'").Replace("&#x2B;", "+").Replace("&quot;", "\"").Replace("&#xB7;", "·").Replace("&#x2022;", "·").Replace("&mdash;"," ").Replace("&#xA0;"," ").Replace("&#xE4;","ä").Replace("&#xD6;", "Ö").Replace("&#xFC;", "ü").Replace("&#x2013;","-").Replace("&#x2580;", "ss").Replace("&#xF6;", "ö");
            }
           
            return replaced;
        }
        private static DateTime? GetDateFromString(string text)
        {
            DateTime? outputDate = null;
            if (String.IsNullOrEmpty(text))
            {
                return outputDate.GetValueOrDefault();
            }
            outputDate = DateTime.Parse(text, new CultureInfo("en-US"), DateTimeStyles.NoCurrentDateDefault);
            return outputDate.GetValueOrDefault();
        }

        public string Sanitize(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            doc.DocumentNode.Descendants()
                            .Where(n => n.Name == "script" || n.Name == "style" || n.Name == "svg" || n.Name == "header" || n.Name == "input")
                            .ToList()
                            .ForEach(n => n.Remove());
           
            doc.GetElementbyId("share-email-popup")?.Remove();
            doc.GetElementbyId("catalog-footer")?.Remove();
            doc.GetElementbyId("hide-footer")?.Remove();
            doc.GetElementbyId("image-viewer")?.Remove();
            doc.GetElementbyId("frequently-purchased")?.Remove();
            doc.GetElementbyId("related-products-container")?.Remove();
            doc.GetElementbyId("featured-products-container")?.Remove();
            
 
            var sections = doc.DocumentNode.SelectNodes("//section");
            if (sections != null)
            {
                foreach (var tag in sections)
                {
                    
                    if (tag.Attributes["class"] != null && string.Compare(tag.Attributes["class"].Value, "column fluid product-description-title visible-xs visible-sm", StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
                        tag.Remove();
                    }
                    if (tag.Attributes["class"] != null && string.Compare(tag.Attributes["class"].Value, "column fluid product-description-title hidden-xs hidden-sm", StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
                        tag.Remove();
                    }
                    if (tag.Attributes["class"] != null && string.Compare(tag.Attributes["class"].Value, "column fluid product-attribute-grouping", StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
                        tag.Remove();
                    }
                    if (tag.Attributes["class"] != null && string.Compare(tag.Attributes["class"].Value, "column fluid volume-discount-container-legacy hidden-xs hidden-sm", StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
                        tag.Remove();
                    }
                    if (tag.Attributes["class"] != null && string.Compare(tag.Attributes["class"].Value, "column fluid product-description-ranking", StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
                        tag.Remove();
                    }
                    if (tag.Attributes["class"] != null && string.Compare(tag.Attributes["class"].Value, "column action-fixed", StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
                        tag.Remove();
                    }
                }
            }

            var text = Regex.Replace(doc.DocumentNode.InnerHtml, @"\s+", " ");
            return text.Trim();


        }

        public string GetUrl(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            var url = doc.DocumentNode.SelectSingleNode("//article[contains(@class,'ga-product')]").GetAttributeValue("itemid", null);
            return url;
        }
        public string Get1stCategory(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            string category = null;
            var breadCrumbs = doc.GetElementbyId("breadCrumbs").SelectNodes("//a[contains(@class,'last')]");
            if (breadCrumbs != null)
            {
                if (breadCrumbs.Count > 1)
                {
                    category = doc.GetElementbyId("breadCrumbs").SelectNodes("//a[contains(@class,'last')]")?.ElementAt(1)?.InnerText ?? null;
                }
                if (breadCrumbs.Count == 1)
                {
                    category = doc.GetElementbyId("breadCrumbs").SelectNodes("//a[contains(@class,'last')]")?.ElementAt(0)?.InnerText ?? null;
                }
            }
           
            return FixEncoding(category);
        }
        public string Get2ndCategory(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            string category = null;
            var breadCrumbs = doc.GetElementbyId("breadCrumbs").SelectNodes("//a[contains(@class,'last')]");
            if (breadCrumbs != null)
            {
                if (breadCrumbs.Count > 2)
                {
                    category = doc.GetElementbyId("breadCrumbs").SelectNodes("//a[contains(@class,'last')]")?.ElementAt(2)?.InnerText ?? null;
                }
               
            }

            return FixEncoding(category);
        }
    }
}
