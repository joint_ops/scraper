﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WebScraper.Core.Domain.Settings;

namespace WebScraper.Infra.Data.Configuration
{
    public class HostConfiguration
    {
        public static void Setup(ModelBuilder builder)
        {
            builder.Entity<Host>()
                .HasKey(p => p.Id);


            builder.Entity<Host>()
                .ToTable("Hosts");
        }
    }
}
