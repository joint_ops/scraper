﻿using Microsoft.EntityFrameworkCore;
using WebScraper.Core.Domain.Products;

namespace WebScraper.Infra.Data.Configuration
{
    public class ProductImageConfiguration
    {
        public static void Setup(ModelBuilder builder)
        {
            builder.Entity<ProductImage>()
                .HasKey(pi => pi.Id);

            builder.Entity<ProductImage>()
               .HasOne(pi => pi.Product);

            builder.Entity<ProductImage>()
                .Property(pi => pi.Url)
                .IsRequired()
                .HasMaxLength(255);

            builder.Entity<ProductImage>()
                .ToTable("ProductImages");

        }
    }
}
