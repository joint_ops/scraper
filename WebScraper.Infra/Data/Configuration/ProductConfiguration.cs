﻿using Microsoft.EntityFrameworkCore;
using WebScraper.Core.Domain.Products;

namespace WebScraper.Infra.Data.Configuration
{
    public class ProductConfiguration
    {
        public static void Setup(ModelBuilder builder)
        {
            builder.Entity<Product>()
                .HasKey(p => p.Id);
           

            builder.Entity<Product>()
              .HasOne(p => p.Company);

            builder.Entity<Product>()
               .HasMany(p => p.ProductImages)
               .WithOne(c => c.Product)
               .HasForeignKey(p => p.ProductId);

           
            builder.Entity<Product>()
                .ToTable("Products");
        }
    }
}
