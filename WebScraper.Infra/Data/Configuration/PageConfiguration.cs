﻿using Microsoft.EntityFrameworkCore;
using WebScraper.Core.Domain.Companies;

namespace WebScraper.Infra.Data.Configuration
{
    public class PageConfiguration
    {
        public static void Setup(ModelBuilder builder)
        {
            builder.Entity<Page>()
                .HasKey(p => p.Id);

            builder.Entity<Page>()
              .HasOne(p => p.Company);

            builder.Entity<Page>()
                .ToTable("Pages");
        }
    }
}
