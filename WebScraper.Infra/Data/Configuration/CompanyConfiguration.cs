﻿using Microsoft.EntityFrameworkCore;
using System;
using WebScraper.Core.Domain.Companies;

namespace WebScraper.Infra.Data.Configuration
{
    public class CompanyConfiguration
    {
        public static void Setup(ModelBuilder builder)
        {
           
            builder.Entity<Company>()
               .HasKey(c => c.Id);

            builder.Entity<Company>()
               .HasIndex(c => c.Name)
               .IsUnique();

            builder.Entity<Company>()
               .HasMany(c => c.Products)
               .WithOne(p => p.Company)
               .HasForeignKey(c => c.CompanyId);

            builder.Entity<Company>()
              .HasMany(c => c.Pages)
              .WithOne(p => p.Company);

            builder.Entity<Company>()
                .Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(255);

            builder.Entity<Company>().HasData(
                 new Company(Guid.NewGuid(),"firma testowa-usuń po starcie")); 


            builder.Entity<Company>()
                .ToTable("Companies");


        }
    }
}
