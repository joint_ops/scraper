﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WebScraper.Core.Domain.Settings;

namespace WebScraper.Infra.Data.Configuration
{
    public class SettingConfiguration
    {
        public static void Setup(ModelBuilder builder)
        {
            builder.Entity<Setting>()
                .HasKey(p => p.Id);


            builder.Entity<Setting>()
                .ToTable("Settings");
        }
    }
}
