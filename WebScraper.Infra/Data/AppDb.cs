﻿using Microsoft.EntityFrameworkCore;
using WebScraper.Infra.Data.Configuration;
using WebScraper.Core.Domain.Products;
using WebScraper.Core.Domain.Companies;
using Microsoft.EntityFrameworkCore.Storage;
using System.Threading.Tasks;
using System.Data;
using WebScraper.Core.Domain.Settings;
using Microsoft.Extensions.Configuration;
using System.IO;
using System;
using System.Diagnostics;

namespace WebScraper.Infra.Data
{
    public class AppDb : DbContext
    {
        private IDbContextTransaction _currentTransaction;
        public AppDb()
        { }
        public AppDb(DbContextOptions<AppDb> options) : base()
        {
        }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Host> Hosts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json",optional: false, reloadOnChange: true)
                   .Build();

                var connectionString = configuration.GetConnectionString("Sqlite");

                optionsBuilder.UseSqlite(connectionString);

            }
            optionsBuilder.EnableSensitiveDataLogging();
        }
        

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            ProductConfiguration.Setup(builder);
            CompanyConfiguration.Setup(builder);
            ProductImageConfiguration.Setup(builder);
            PageConfiguration.Setup(builder);
            SettingConfiguration.Setup(builder);
        }
        public async Task BeginTransactionAsync()
        {
            if (_currentTransaction != null)
            {
                return;
            }

            _currentTransaction = await Database.BeginTransactionAsync(IsolationLevel.ReadCommitted).ConfigureAwait(false);
        }
        public async Task CommitTransactionAsync()
        {
            try
            {
                await SaveChangesAsync().ConfigureAwait(false);

                _currentTransaction?.Commit();
            }
            catch
            {
                RollbackTransaction();
                throw;
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }
        public void RollbackTransaction()
        {
            try
            {
                _currentTransaction?.Rollback();
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }
    }

}

