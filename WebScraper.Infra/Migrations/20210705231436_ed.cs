﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebScraper.Infra.Migrations
{
    public partial class ed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("82e25e98-1cfb-404b-98ac-7484307a0343"));

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Settings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Currency",
                table: "Settings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Language",
                table: "Settings",
                nullable: true);

            migrationBuilder.InsertData(
                table: "Companies",
                columns: new[] { "Id", "CanBeUpdated", "CheckedAt", "CreatedAt", "HasCompletedImages", "HasCompletedProducts", "Name", "UpdatedAt", "UpdatedTimes" },
                values: new object[] { new Guid("22afe2bc-18bf-4310-9182-5e60273571d1"), false, null, new DateTime(2021, 7, 5, 23, 14, 35, 543, DateTimeKind.Utc).AddTicks(5198), false, false, "firma testowa-usuń po starcie", null, 0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("22afe2bc-18bf-4310-9182-5e60273571d1"));

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "Currency",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "Language",
                table: "Settings");

            migrationBuilder.InsertData(
                table: "Companies",
                columns: new[] { "Id", "CanBeUpdated", "CheckedAt", "CreatedAt", "HasCompletedImages", "HasCompletedProducts", "Name", "UpdatedAt", "UpdatedTimes" },
                values: new object[] { new Guid("82e25e98-1cfb-404b-98ac-7484307a0343"), false, null, new DateTime(2020, 4, 17, 11, 16, 36, 963, DateTimeKind.Utc).AddTicks(9912), false, false, "firma testowa-usuń po starcie", null, 0 });
        }
    }
}
