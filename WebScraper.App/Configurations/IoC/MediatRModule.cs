﻿using System.Reflection;
using Autofac;
using MediatR;


namespace WebScraper.App.Configurations.IoC
{
    internal class MediatRModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Mediator>().As<IMediator>().InstancePerLifetimeScope();

            builder
                .Register<ServiceFactory>(ctx =>
                {
                    var c = ctx.Resolve<IComponentContext>();
                    return t => c.TryResolve(t, out var o) ? o : null;
                })
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(MediatRModule).GetTypeInfo().Assembly).AsImplementedInterfaces();         

        }
    }
}