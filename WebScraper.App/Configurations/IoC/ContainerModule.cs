﻿using Autofac;
using WebScraper.App.Configurations.Mappings;

namespace WebScraper.App.Configurations.IoC
{
    public class ContainerModule : Module
    {      
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(AutoMapperConfig.Initialize()).SingleInstance();
            builder.RegisterModule(new MediatRModule());
            builder.RegisterModule(new WindowModule());
        }
    }
}
