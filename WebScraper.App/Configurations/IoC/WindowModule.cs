﻿using Autofac;

namespace WebScraper.App.Configurations.IoC
{
    internal class WindowModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MainWindow>().AsSelf().InstancePerDependency();
        }
    }
}
