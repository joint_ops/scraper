﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace WebScraper.App.Configurations.Mappings
{
    public static class AutoMapperConfig
    {
        public static IMapper Initialize()
        {
            var config = new MapperConfiguration(cfg =>
            {
                var assembly = typeof(AutoMapperConfig)
                    .GetTypeInfo()
                    .Assembly;

                var mappingProfiles = assembly.GetTypes()
                    .Where(type => typeof(Profile).IsAssignableFrom(type))
                    .ToList();

                cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
                cfg.AllowNullCollections = true;
                cfg.AllowNullDestinationValues = true;
                cfg.AddMaps(mappingProfiles);
            });

            return config.CreateMapper();
        }
    }
}
