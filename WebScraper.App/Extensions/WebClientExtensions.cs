﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebScraper.App.Extensions
{
    public static class WebClientExtensions
    {
        public async static Task<string> DownloadStringAwareOfEncoding(this WebClient webClient, string url)
        {
            var uri = new Uri(url);
            var rawData = await webClient.DownloadDataTaskAsync(uri);
            var encoding = WebUtils.GetEncodingFrom(webClient.ResponseHeaders, Encoding.UTF8);
            return encoding.GetString(rawData);
        }
    }
}
