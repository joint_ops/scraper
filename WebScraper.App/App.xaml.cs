﻿using System;
using System.Windows;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using WebScraper.Core.Services;
using WebScraper.Infra.Services;
using MediatR;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using WebScraper.App.Configurations.IoC;
using WebScraper.App.Modules.Companies;
using WebScraper.Infra.Data;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using Quartz;
using System.Threading.Tasks;
using Quartz.Impl;

namespace WebScraper.App
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public IContainer ApplicationContainer { get; private set; }
        public IServiceProvider ServiceProvider { get; private set; }
        public IConfiguration Configuration { get; private set; }

        protected async override void OnStartup(StartupEventArgs e)         
        {
            var builder = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            Configuration = builder.Build();

            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

        }

       
        private IServiceProvider ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<AppDb>(options =>
                          options.UseSqlite(Configuration.GetConnectionString("Sqlite")));

            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);
            containerBuilder.RegisterModule(new ContainerModule());
            containerBuilder.RegisterModule(new Infra.Configurations.IoC.ContainerModule(Configuration));
            ApplicationContainer = containerBuilder.Build();

            var mainWindow = ApplicationContainer.Resolve<MainWindow>();
            mainWindow.Show();
           

            return new AutofacServiceProvider(ApplicationContainer);
        }

        public App()
        {
            Dispatcher.UnhandledException += OnDispatcherUnhandledException;
         
        }

        void OnDispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            string errorMessage = string.Format("An unhandled exception occurred: {0}", e.Exception.Message);
            MessageBox.Show(errorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            e.Handled = true;
        }

       
       


    }
}
