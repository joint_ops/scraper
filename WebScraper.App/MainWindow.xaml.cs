﻿using MediatR;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebScraper.App.Modules.Bulk;
using WebScraper.App.Modules.Companies;
using WebScraper.App.Modules.Products;
using WebScraper.App.Modules.Settings;
using WebScraper.Core.Services;

namespace WebScraper.App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IMediator _mediator;
        public MainWindow(IMediator mediator)
        {
            InitializeComponent();
            _mediator = mediator;
            GridMain.Children.Add(new UserControlCompaniesList(_mediator));
            MouseDown += Window_MouseDown;
            HideLoadingIcon();
            CheckHost();
        }
        private async void CheckHost()
        {
            var exist = await _mediator.Send(new CheckHost.Query() { Name = Environment.MachineName });
            if (!exist)
            {
                MessageBox.Show("Brak uprawnień");
                Application.Current.Shutdown();
            }

        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }
        public void ShowLoadingIcon()
        {
            LoadingIcon.Visibility = Visibility.Visible;
        }
        public void HideMenu()
        {
            ListViewMenu.Visibility = Visibility.Hidden;
            ButtonOpenMenu.Visibility = Visibility.Hidden;
            Help.Visibility = Visibility.Hidden;
            Settings.Visibility = Visibility.Hidden;

        }
        public void ShowMenu()
        {
            ListViewMenu.Visibility = Visibility.Visible;
            ButtonOpenMenu.Visibility = Visibility.Visible;
            Help.Visibility = Visibility.Visible;
            Settings.Visibility = Visibility.Visible;
        }

        public void HideLoadingIcon()
        {
            LoadingIcon.Visibility = Visibility.Hidden;
        }
        void Settings_Click(object sender, RoutedEventArgs e)
        {
            GridMain.Children.Clear();
            var usc = new UserControlSettings(_mediator);
            GridMain.Children.Add(usc);
        }
        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Visible;
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
        }
        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
            ButtonOpenMenu.Visibility = Visibility.Visible;
        }
        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
            
        }
        private void Minimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
        private void Maximize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Maximized;
        }
        private void Window_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Normal;
        }
        private void Help_Click(object sender, RoutedEventArgs e)
        {
            ShowLoadingIcon();
            HideMenu();
            GridMain.Children.Add(new UserControlHelp());
            ShowMenu();
            HideLoadingIcon();
        }
        private async void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UserControl usc = null;
           

            switch (((ListViewItem)((ListView)sender).SelectedItem).Name)
            {
                case "ItemHome":
                    GridMain.Children.Clear();
                    ShowLoadingIcon();
                    HideMenu();
                    usc = new UserControlCompaniesList(_mediator);                    
                    GridMain.Children.Add(usc);
                    ShowMenu();
                    HideLoadingIcon();
                    break;

                case "ItemCreate":
                    GridMain.Children.Clear();
                    ShowLoadingIcon();
                    HideMenu();
                    usc = new UserControlAddCompany(_mediator);
                    GridMain.Children.Add(usc);
                    ShowMenu();
                    HideLoadingIcon();
                    break;
                case "ItemXLs":
                    ShowLoadingIcon();
                    HideMenu();
                    await _mediator.Send(new Modules.Products.CreateAllProductsExcelFile.Command());
                    ShowMenu();
                    HideLoadingIcon();
                    break;
                case "Bulk":
                    GridMain.Children.Clear();                  
                    usc = new UserControlBulkUpdate(_mediator);
                    GridMain.Children.Add(usc);                   
                    break;
                case "BulkProducts":
                    GridMain.Children.Clear();
                    usc = new UserControlBulk(_mediator);
                    GridMain.Children.Add(usc);
                    break;

                default:
                    break;
            }

        }

    }



}
   

