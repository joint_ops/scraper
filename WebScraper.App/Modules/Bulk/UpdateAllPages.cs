﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.App.Modules.Companies;
using WebScraper.Core.Domain.Companies;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Bulk
{
    public static class UpdateAllPages
    {
        public class Command : IRequest<int>
        {
          public List<GetAllPages.Result.Page> Pages { get; set; }

        }
     
        public class Handler : IRequestHandler<Command, int>
        {
            private readonly AppDb _db;

            public Handler(AppDb db)
            {
                _db = db;
            }

            public async Task<int> Handle(Command command, CancellationToken token)
            {
               

                var products = await _db.Products.ToListAsync(token);
                var pages = await _db.Pages.ToListAsync(token);

                var setting = await _db.Settings
                    .AsNoTracking()
                    .FirstOrDefaultAsync();

                if (setting.ImagesPath == null)
                {
                    setting.ImagesPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                }

                if (pages != null)
                {
                    _db.Pages.RemoveRange(pages);
                }
                if (products != null)
                {
                    _db.Products.RemoveRange(products);
                }

                foreach (var page in command.Pages)
                {
                    _db.Pages.Add(new Page(page.CompanyId, page.Html, page.Url));
                    var company = _db.Companies.FirstOrDefault(c => c.Id == page.CompanyId);
                    company.SetCheckedAt();
                    company.SetHasCompletedProducts(false);
                    company.SetHasCompletedImages(false);

                    string imagesDirectory = $@"{setting.ImagesPath}\Images\{company.Name}";
                    if (Directory.Exists(imagesDirectory))
                    {
                        Directory.Delete(imagesDirectory, true);
                    }
                }

              
                await _db.CommitTransactionAsync();

                return command.Pages.Count;
            }

        }
    }
}
