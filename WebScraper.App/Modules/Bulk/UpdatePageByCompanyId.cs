﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.App.Modules.Companies;
using WebScraper.Core.Domain.Companies;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Bulk
{
    public static class UpdatePageByCompanyId
    {
        public class Command : IRequest<string>
        {
            public Guid CompanyId { get; set; }
            public string Url { get; set; }
            public string Html { get; set; }

        }

        public class Handler : IRequestHandler<Command, string>
        {
            private readonly AppDb _db;

            public Handler(AppDb db)
            {
                _db = db;
            }

            public async Task<string> Handle(Command command, CancellationToken token)
            {
                await Task.Run(async () =>
                {
                    _db.Pages.Add(new Page(command.CompanyId, command.Html, command.Url));
                     await _db.SaveChangesAsync();
                });
                return command.Url;
            }

        }
    }
}
