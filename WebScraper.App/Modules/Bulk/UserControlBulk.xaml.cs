﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebScraper.App.Modules.Companies;
using WebScraper.App.Modules.Products;
using WebScraper.App.Modules.Settings;

namespace WebScraper.App.Modules.Bulk
{
    /// <summary>
    /// Interaction logic for UserControlBulk.xaml
    /// </summary>
    public partial class UserControlBulk : UserControl
    {
        private readonly IMediator _mediator;
        CancellationTokenSource cts = new CancellationTokenSource();

        public UserControlBulk(IMediator mediator)
        {
            _mediator = mediator;
            InitializeComponent();
            ExecuteAsync();
        }

        private async void ExecuteAsync()
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            mainWindow.ShowLoadingIcon();
            mainWindow.HideMenu();
            var progress = new Progress<ProgressReportModel>();

            try
            {
                var watch = Stopwatch.StartNew();
                ActionText.Text = $"Pobieranie stron producentów ";
                progress.ProgressChanged += ReportProgress;
                var pages = await DownloadPagesAsync(progress, cts.Token);
                Result.Content = $"Pobrano {pages.Count} stron producentów, zapisywanie w toku...";

                ActionText.Text = "Zapisywanie stron produktów do bazy danych";
                var updated = await UpdatePagesAsync(pages, progress, cts.Token);
                Result.Content = $"Zapisano {updated.Count} stron produktów.";
                DownloadProgress.Value = 0;

                ActionText.Text = $"Pobieranie produktów ";
                var companies = await RunDownloadAsync(progress, cts.Token);
                Result.Content = $"Pobrano produkty {companies.Count} producentów, zapisywanie w toku...";

                ActionText.Text = "Zapisywanie produktów do bazy danych";
                var added = await AddHtml(companies, progress, cts.Token);

                Result.Content = $"Zapisano produkty {added.Count} producentów.";

                
                watch.Stop();
                Result.Content = $"Zakończono, czas wykonania: { watch.Elapsed.TotalMinutes } minut. ";
                mainWindow.HideLoadingIcon();
                mainWindow.ShowMenu();

            }
            catch (OperationCanceledException)
            {
                ActionText.Text = "Proces anulowano";
                Result.Content = $"Zakończono,proces anulowany przez użytkownika.";
                DownloadProgress.Value = 100;
                mainWindow.HideLoadingIcon();
                mainWindow.ShowMenu();
            }

            CancelButton.Visibility = Visibility.Hidden;
            GoBackButton.Visibility = Visibility.Visible;


        }
        private async Task<List<string>> UpdatePagesAsync(List<GetAllPagesThatCanBeUpdated.Result.Page> data, IProgress<ProgressReportModel> progress, CancellationToken cancellationToken)
        {
            await _mediator.Send(new ClearTablesThatCanBeUpdated.Command());
            ProgressReportModel report = new ProgressReportModel();
            var output = new List<string>();
            foreach (var item in data)
            {
                var result = await _mediator.Send(new UpdatePageByCompanyId.Command() { CompanyId = item.CompanyId, Url = item.Url, Html = item.Html });
                output.Add(result);
                cancellationToken.ThrowIfCancellationRequested();
                report.PercentageComplete = (output.Count * 100) / data.Count;
                progress.Report(report);
                Result.Content = $"Zapisywanie strony {result}";
            }
            await _mediator.Send(new SetCompaniesStatus.Command());
            return output;
        }
        private async Task<List<GetAllPagesThatCanBeUpdated.Result.Page>> DownloadPagesAsync(IProgress<ProgressReportModel> progress, CancellationToken cancellationToken)
        {
            ProgressReportModel report = new ProgressReportModel();
            var data = await _mediator.Send(new GetAllPagesThatCanBeUpdated.Query());
            var output = new List<GetAllPagesThatCanBeUpdated.Result.Page>();

            for (int i = 0; i < data.Pages.Count; i++)
            {
                var result = await DownloadWebsiteAsync(data.Pages[i].Url);
                var pageData = new GetAllPagesThatCanBeUpdated.Result.Page();
                pageData.Html = result.Html;
                pageData.Url = data.Pages[i].Url;
                pageData.CompanyId = data.Pages[i].CompanyId;
                pageData.Id = data.Pages[i].Id;

                output.Add(pageData);
                cancellationToken.ThrowIfCancellationRequested();
                report.PercentageComplete = (output.Count * 100) / data.Pages.Count;
                progress.Report(report);
                Result.Content = $"Pobieranie stron z produktami firmy {data.Pages[i].Company.Name}{Environment.NewLine}{result.Url} ";

            }

            return output;
        }
        private async Task<List<int>> AddHtml(List<GetAllProductsLinksThatCanBeUpdated.Company> data, IProgress<ProgressReportModel> progress, CancellationToken cancellationToken)
        {
            ProgressReportModel report = new ProgressReportModel();
            var output = new List<int>();

            foreach (var item in data)
            {
                var result = await _mediator.Send(new AddCompanyProducts.Command() { CompanyId = item.CompanyId, HtmlList = item.HtmlList });
                output.Add(result);

                cancellationToken.ThrowIfCancellationRequested();
                report.PercentageComplete = (output.Count * 100) / data.Count;
                progress.Report(report);
                Result.Content = $"Zapisywanie produktów firmy {item.Name}";
            }

            return output;
        }

        private async Task<List<GetAllProductsLinksThatCanBeUpdated.Company>> RunDownloadAsync(IProgress<ProgressReportModel> progress, CancellationToken cancellationToken)
        {
            var data = await _mediator.Send(new GetAllProductsLinksThatCanBeUpdated.Query());

            var companies = new List<GetAllProductsLinksThatCanBeUpdated.Company>();
            ProgressReportModel report = new ProgressReportModel();

            for (int i = 0; i < data.Companies.Count; i++)
            {
                var company = new GetAllProductsLinksThatCanBeUpdated.Company();
                company.CompanyId = data.Companies[i].CompanyId;
                company.Name = data.Companies[i].Name;

                foreach (var link in data.Companies[i].ProductsLinks)
                {
                    var result = await DownloadWebsiteAsync(link);
                    company.HtmlList.Add(result.Html);
                    cancellationToken.ThrowIfCancellationRequested();
                    Result.Content = $"Pobieranie produktów firmy {data.Companies[i].Name}{Environment.NewLine}{result.Url} ";
                }

                companies.Add(company);

                report.PercentageComplete = (companies.Count * 100) / data.Companies.Count;
                progress.Report(report);
            }

            return companies;
        }
        private async Task<Dldata> DownloadWebsiteAsync(string url)
        {
            var client = new WebClient();
            client.Encoding = Encoding.UTF8;
            client.Headers.Add(HttpRequestHeader.UserAgent, "");
            var settings = await _mediator.Send(new GetLast.Query());
            string firstCookieString = null;
            string secondCookieString = null;

            if (String.IsNullOrEmpty(settings.Language) || String.IsNullOrEmpty(settings.Currency) || String.IsNullOrEmpty(settings.Country))
            {
                firstCookieString = "ih-preference=store=0&country=PL&language=en-US&currency=USD;";
                secondCookieString = "iher-pref1=storeid=0&sccode=PL&lan=en-US&scurcode=USD&wp=1&lchg=1&ifv=1&bi=0";
            }
            else
            {
                firstCookieString = $"ih-preference=store=0&country={settings.Country}&language={settings.Language}&currency={settings.Currency};";
                secondCookieString = $"iher-pref1=storeid=0&sccode={settings.Country}&lan={settings.Language}&scurcode={settings.Currency}&wp=1&lchg=1&ifv=1&bi=0";
            }
            client.Headers.Add(HttpRequestHeader.Cookie, firstCookieString + secondCookieString);
            var output = new Dldata();
            output.Url = url;
            try
            {
                output.Html = await client.DownloadStringTaskAsync(url);
            }
            catch (Exception ex)
            {
                Errors.Text += $"{Environment.NewLine}{ex.Message} {Environment.NewLine}{url} {Environment.NewLine}";
                ErrorCount.Content = int.Parse(ErrorCount.Content.ToString()) + 1;
            }

            return output;
        }
        private void goBackButton_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            var companiesList = new UserControlCompaniesList(_mediator);
            mainWindow.GridMain.Children.Clear();
            mainWindow.GridMain.Children.Add(companiesList);
        }
        private void cancelOperation_Click(object sender, RoutedEventArgs e)
        {
            cts.Cancel();
        }


        private void ReportProgress(object sender, ProgressReportModel e)
        {
            DownloadProgress.Value = e.PercentageComplete;
        }


       
        private class Dldata
        {
            public string Html { get; set; }
            public string Url { get; set; }

        }
        private class Pagedata
        {
            public Guid Id { get; set; }
            public Guid CompanyId { get; set; }
            public string Html { get; set; }
            public string Url { get; set; }

        }
        private class Outdata
        {
            public List<string> HtmlList { get; set; } = new List<string>();
            public Guid CompanyId { get; set; }

        }

        private class ProgressReportModel
        {
            public int PercentageComplete { get; set; } = 0;
        }
       
    }
}
