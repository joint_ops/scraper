﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Bulk
{
    public static class ClearTablesThatCanBeUpdated
    {
        public class Command : IRequest<int>
        {

        }

        public class Handler : IRequestHandler<Command, int>
        {
            private readonly AppDb _db;

            public Handler(AppDb db)
            {
                _db = db;
            }

            public async Task<int> Handle(Command command, CancellationToken token)
            {
                var products = await _db.Products.Where(p => p.Company.CanBeUpdated).ToListAsync(token);
                var pages = await _db.Pages.Where(p => p.Company.CanBeUpdated).ToListAsync(token);
                var companies = await _db.Companies.Where(c => c.CanBeUpdated).ToListAsync(token);

                var setting = await _db.Settings
                    .AsNoTracking()
                    .FirstOrDefaultAsync();

                if (setting.ImagesPath == null)
                {
                    setting.ImagesPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                }

                if (pages != null)
                {
                    _db.Pages.RemoveRange(pages);
                }
                if (products != null)
                {
                    _db.Products.RemoveRange(products);
                }

                await _db.CommitTransactionAsync();

                foreach (var company in companies)
                {
                    string imagesDirectory = $@"{setting.ImagesPath}\Images\{company.Name}";
                    if (Directory.Exists(imagesDirectory))
                    {
                        Directory.Delete(imagesDirectory, true);
                    }
                }

                return companies.Count;
            }

        }
    }
}
