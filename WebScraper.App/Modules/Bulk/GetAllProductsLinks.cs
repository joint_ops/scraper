﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Infra.Data;
using WebScraper.Core.Services;

namespace WebScraper.App.Modules.Bulk
{
    public static class GetAllProductsLinks
    {
        public class Query : IRequest<Result>
        {

        }
        public class Result
        {
            public List<Company> Companies { get; set; }

            public Result()
            {
                Companies = new List<Company>();
            }

        }
        public class Company
        {
            public Guid CompanyId { get; set; }
            public string Name { get; set; }
            public List<string> ProductsLinks { get; set; }
            public List<string> HtmlList { get; set; } = new List<string>();
        }

        public class Handler : IRequestHandler<Query, Result>
        {
            private readonly AppDb _db;
            private readonly IMapper _mapper;
            private readonly IScraperService _scraperService;

            public Handler(AppDb db, IMapper mapper, IScraperService scraperService)
            {
                _db = db;
                _mapper = mapper;
                _scraperService = scraperService;
            }

            public async Task<Result> Handle(Query query, CancellationToken token)
            {
                var companies = await _db.Companies
                    .AsNoTracking()
                    .OrderBy(p => p.Name)
                    .Include(p => p.Pages)
                    .ToListAsync(token);


                var result = new Result();

                foreach (var company in companies)
                {
                    var links = new List<string>();

                    foreach (var page in company.Pages)
                    {
                        foreach (var link in _scraperService.GetProductsUrls(page.Html))
                        {
                            links.Add(link);
                        }
                    }

                    var companyResult = new Company() { CompanyId = company.Id,Name = company.Name ,ProductsLinks = links};
                    result.Companies.Add(companyResult);
                }

                return result;
            }



        }
    }
}
