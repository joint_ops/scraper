﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Bulk
{
    public static class SetCompaniesStatus
    {
        public class Command : IRequest<int>
        {
          
        }

        public class Handler : IRequestHandler<Command, int>
        {
            private readonly AppDb _db;

            public Handler(AppDb db)
            {
                _db = db;
            }

            public async Task<int> Handle(Command command, CancellationToken token)
            {
                var companies = await _db.Companies.Where(c => c.CanBeUpdated).ToListAsync(token);

                foreach (var company in companies)
                {
                    company.SetCheckedAt();
                    company.SetHasCompletedProducts(false);
                    company.SetHasCompletedImages(false);
                }
                await _db.CommitTransactionAsync();

                return companies.Count;
            }

        }
    }
}
