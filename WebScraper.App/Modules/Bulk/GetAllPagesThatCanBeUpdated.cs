﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Core.Domain.Companies;
using WebScraper.Core.Domain.Products;
using WebScraper.Core.Services;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Bulk
{
    public static class GetAllPagesThatCanBeUpdated
    {
        public class Query : IRequest<Result>
        {

        }
        public class Result
        {
            public List<Page> Pages { get; set; }
            public class Page
            {
                public Guid Id { get; set; }
                public Guid CompanyId { get; set; }
                public string Url { get; set; }
                public string Html { get; set; }
                public Company Company { get; set; }
            }
            public class Company
            {
                public Guid Id { get; set; }
                public string Name { get; set; }
            }
        }

        public class Handler : IRequestHandler<Query, Result>
        {
            private readonly AppDb _db;
            private readonly IMapper _mapper;

            public Handler(AppDb db,IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }
            public async Task<Result> Handle(Query query, CancellationToken token)
            {
                var pages = await _db.Pages
                    .AsNoTracking()
                    .Where( p =>p.Company.CanBeUpdated)
                    .Include(p => p.Company)
                    .OrderBy(p => p.Company.Name)
                    .ProjectTo<Result.Page>(_mapper.ConfigurationProvider)
                    .ToListAsync(token);

                return new Result() { Pages = pages };
            }
            public class MappingProfile : Profile
            {
                public MappingProfile()
                {
                    CreateMap<Page, Result.Page>();
                    CreateMap<Company, Result.Company>();
                }
            }


        }
    }
}
