﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using WebScraper.Core.Domain.Companies;
using WebScraper.Core.Domain.Products;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Companies
{
    public static class SetCanNotBeUpdated
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Command, Unit>
        {
            private readonly AppDb _db;

            public Handler(AppDb db)
            {
                _db = db;
            }
            public async Task<Unit> Handle(Command command, CancellationToken token)
            {
                var company = await _db.Companies.FirstOrDefaultAsync(c => c.Id == command.Id, token);
                if (company == null)
                {
                    MessageBox.Show("company is null");
                }

                company.SetCanBeUpdated(false);
                await _db.SaveChangesAsync(token);
                return Unit.Value;
            }

        }
    }
}
