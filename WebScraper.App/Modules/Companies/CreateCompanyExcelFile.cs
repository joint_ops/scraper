﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Core.Domain.Companies;
using WebScraper.Core.Domain.Products;
using WebScraper.Core.Helpers;
using WebScraper.Core.Services;
using WebScraper.Infra.Data;


namespace WebScraper.App.Modules.Companies
{
    public static class CreateCompanyExcelFile
    {
        public class Command : IRequest<string>
        {          
            public Guid Id { get; set; }
        }
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
               
            }
        }
        public class Result
        {
            public string Name { get; set; }
            public List<Product> Products { get; set; }

            public Result()
            {
                Products = new List<Product>();
            }

            public class Product
            {
                public Guid Id { get; set; }
                public string Name { get; set; }
                public string Url { get; set; }
                public string StockStatus { get; set; }
                public decimal Price { get; set; }
                public string Currency { get; set; }
                public string Quantity { get; set; }
                public string UpcCode { get; set; }
                public string ProductCode { get; set; }
                public string Dimensions { get;  set; }
                public string Weight { get;  set; }
                public DateTime? ExpirationDate { get; set; }
                public string DescriptionList { get; set; }
                public string Ingredients { get; set; }
                public string SuggestedUse { get; set; }
                public string FirstCategory { get; set; }
                public string SecondCategory { get;  set; }
                public string Brand { get; set; }
                public string Producer { get; set; }
            }

        }
        public class Handler : IRequestHandler<Command, string>
        {
            private readonly AppDb _db;
            private readonly IMapper _mapper;
            public Handler(AppDb db,IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }

            public async Task<string> Handle(Command command, CancellationToken token)
            {
                var company = await _db.Companies
                    .AsNoTracking()                                 
                    .Where(p => p.Id == command.Id)
                    .Include(p => p.Products)
                    .ProjectTo<Result>(_mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(token);

                var setting = await _db.Settings.AsNoTracking().FirstOrDefaultAsync(token);
                if (setting.XlsPath == null)
                {
                    setting.XlsPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                }

                string fileName = @$"{setting.XlsPath}\Excel Files\{company.Name}.xls";

                if (!Directory.Exists($@"{setting.XlsPath}\Excel Files"))
                {
                    Directory.CreateDirectory($@"{setting.XlsPath}\Excel Files");
                }

                var file = File.Create(fileName);

                // create an Excel file objects 
                XSSFWorkbook workbook = new XSSFWorkbook();
                // add a Sheet 
                ISheet sheet = workbook.CreateSheet($"{company.Name}");

                // data is gradually written sheet each row 
                if (company.Products.Count > 0)
                {
                    for (int i = 0; i < company.Products.Count; i++)
                    {
                        IRow rowtemp = sheet.CreateRow(i);
                        rowtemp.CreateCell(0).SetCellValue(company.Products[i].Name);
                        rowtemp.CreateCell(1).SetCellValue(company.Products[i].Quantity);
                        rowtemp.CreateCell(2).SetCellValue(company.Products[i].StockStatus);
                        rowtemp.CreateCell(3).SetCellValue(Decimal.ToDouble(company.Products[i].Price));
                        rowtemp.CreateCell(4).SetCellValue(company.Products[i].Currency);
                        rowtemp.CreateCell(5).SetCellValue(company.Products[i].UpcCode);
                        rowtemp.CreateCell(6).SetCellValue(company.Products[i].ProductCode);
                        rowtemp.CreateCell(7).SetCellValue(company.Products[i].Dimensions);
                        rowtemp.CreateCell(8).SetCellValue(company.Products[i].Weight);
                        rowtemp.CreateCell(9).SetCellValue(company.Products[i].ExpirationDate.ToString());
                        rowtemp.CreateCell(10).SetCellValue(company.Products[i].Brand);
                        rowtemp.CreateCell(11).SetCellValue(company.Products[i].FirstCategory);
                        rowtemp.CreateCell(12).SetCellValue(company.Products[i].SecondCategory);
                        rowtemp.CreateCell(13).SetCellValue(company.Products[i].DescriptionList);
                        rowtemp.CreateCell(14).SetCellValue(company.Products[i].Ingredients);
                        rowtemp.CreateCell(15).SetCellValue(company.Products[i].SuggestedUse);
                    }
                }
               

                workbook.Write(file);

                return fileName;

            }
            public class MappingProfile : Profile
            {
                public MappingProfile()
                {
                    CreateMap<Company, Result>();
                    CreateMap<Product, Result.Product>();
                }
            }

        }
    }
}
