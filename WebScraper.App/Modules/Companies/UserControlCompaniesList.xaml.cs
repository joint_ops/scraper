﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebScraper.App.Modules.Products;

namespace WebScraper.App.Modules.Companies
{
    /// <summary>
    /// Interaction logic for UserControlCompaniesList.xaml
    /// </summary>
    public partial class UserControlCompaniesList : UserControl
    {
        private readonly IMediator _mediator;
       
        public UserControlCompaniesList(IMediator mediator)
        {
            _mediator = mediator;            
            InitializeComponent();
            GetAllCompanies();
        }
        private async void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
           
            var result = await _mediator.Send(new GetBySearchString.Query() { SearchString = SearchBox.Text});
            DataContext = result.Companies;
        }
        void DownloadProducts_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);

            GetAll.Result.Company company = Companies.SelectedItem as GetAll.Result.Company;
            Guid id = company.Id;

            var userControlProduct = new UserControlDownloadProducts(_mediator,id);
            mainWindow.GridMain.Children.Clear();
            mainWindow.GridMain.Children.Add(userControlProduct);
        }
        async void ExportXls_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            mainWindow.ShowLoadingIcon();
            GetAll.Result.Company company = Companies.SelectedItem as GetAll.Result.Company;
            Guid id = company.Id;

            await _mediator.Send(new Modules.Companies.CreateCompanyExcelFile.Command { Id = id });

            mainWindow.HideLoadingIcon();
        }
        void DownloadImages_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            GetAll.Result.Company company = Companies.SelectedItem as GetAll.Result.Company;
            Guid companyId = company.Id;

            var userControlImages = new Modules.Images.UserControlDownloadImages(_mediator, companyId);
            mainWindow.GridMain.Children.Clear();
            mainWindow.GridMain.Children.Add(userControlImages);
        }
        void CreateTables_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            GetAll.Result.Company company = Companies.SelectedItem as GetAll.Result.Company;
            Guid companyId = company.Id;

            var userControlTable = new UserControlCreateTable(_mediator, companyId);
            mainWindow.GridMain.Children.Clear();
            mainWindow.GridMain.Children.Add(userControlTable);
        }
        void Update_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            GetAll.Result.Company company = Companies.SelectedItem as GetAll.Result.Company;
            Guid companyId = company.Id;

            var userControlUpdateCompany = new UserControlUpdateCompany(_mediator, company);
            mainWindow.GridMain.Children.Clear();
            mainWindow.GridMain.Children.Add(userControlUpdateCompany);
        }
        void ShowProducts_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            mainWindow.ShowLoadingIcon();
            GetAll.Result.Company company = Companies.SelectedItem as GetAll.Result.Company;
            Guid companyId = company.Id;

            var productsList = new UserControlCompanyProductsList(_mediator, companyId);

            mainWindow.GridMain.Children.Clear();
            mainWindow.GridMain.Children.Add(productsList);
            mainWindow.HideLoadingIcon();
        }
        async void DeleteCompany_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            GetAll.Result.Company company = Companies.SelectedItem as GetAll.Result.Company;
            Guid id = company.Id;

            string MessageBoxText = "Usuwając tego producenta kasujesz wszystkie powiązane pliki, potwierdzasz ?";
            string Caption = "IHerb WebScraper";

            MessageBoxButton btnMessageBox = MessageBoxButton.YesNo;
            MessageBoxImage icnMessageBox = MessageBoxImage.Warning;

            MessageBoxResult rsltMessageBox = MessageBox.Show(MessageBoxText, Caption, btnMessageBox, icnMessageBox);

            switch (rsltMessageBox)
            {
                case MessageBoxResult.Yes:
                    mainWindow.ShowLoadingIcon();
                    await _mediator.Send(new DeleteCompany.Command() { Id = id });
                    GetAllCompanies();
                    mainWindow.HideLoadingIcon();
                    break;

                case MessageBoxResult.No:
                    break;

            }

        }
        public async void GetAllCompanies()
        {
            var result = await _mediator.Send(new GetAll.Query());
            DataContext = result.Companies;
        }
        async void AddToQueque_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            mainWindow.ShowLoadingIcon();
            GetAll.Result.Company company = Companies.SelectedItem as GetAll.Result.Company;
            Guid companyId = company.Id;

            await _mediator.Send(new SetCanBeUpdated.Command() { Id = company.Id });

            GetAllCompanies();
            mainWindow.HideLoadingIcon();
        }
        async void RemoveQueque_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            mainWindow.ShowLoadingIcon();
            GetAll.Result.Company company = Companies.SelectedItem as GetAll.Result.Company;
            Guid companyId = company.Id;

            await _mediator.Send(new SetCanNotBeUpdated.Command() { Id = company.Id });

            GetAllCompanies();
            mainWindow.HideLoadingIcon();
        }


    }

 
}
