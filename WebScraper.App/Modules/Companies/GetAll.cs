﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Core.Domain.Companies;
using WebScraper.Core.Domain.Products;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Companies
{
    public static class GetAll
    {
        public class Query : IRequest<Result>
        {
          
        }
        public class Result
        {
            public List<Company> Companies { get; set; }
            public Result()
            {
                Companies = new List<Company>();
            }

            public class Company
            {
                public Guid Id { get; set; }
                public string Name { get; set; }
                public string Url { get; set; }
                public int UpdatedTimes { get; set; }
                public bool HasCompletedProducts { get; set; }
                public bool HasCompletedImages { get; set; }
                public bool HasCompletedTables { get; set; }
                public bool CanBeUpdated { get; set; }
                public DateTime CreatedAt { get; set; }
                public DateTime? UpdatedAt { get; set; }
                public DateTime? CheckedAt { get; set; }
                public List<Product> Products { get; set; }
                public List<Page> Pages { get; set; }

            }
            public class Product
            {
                public Guid Id { get; set; }

            }
            public class Page
            {
                public string Url { get; set; }

            }
        }
        public class Handler : IRequestHandler<Query, Result>
        {
            private readonly AppDb _db;
            private readonly IMapper _mapper;

            public Handler(AppDb db, IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }
            public async Task<Result> Handle(Query query, CancellationToken token)
            {
                var companies = await _db.Companies
                    .AsNoTracking()
                    .OrderBy(c => c.Name)
                    .ProjectTo<Result.Company>(_mapper.ConfigurationProvider)
                    .ToListAsync(token);

                foreach (var company in companies)
                {
                    if ((DateTime?)company.CreatedAt == company.UpdatedAt)
                    {
                        company.UpdatedAt = null;
                    }
                }
               
                return new Result() { Companies = companies };
            }
            public class MappingProfile : Profile
            {
                public MappingProfile()
                {
                    CreateMap<Company, Result.Company>();
                    CreateMap<Product, Result.Product>();
                    CreateMap<Page, Result.Page>();
                }
            }

        }
    }
}
