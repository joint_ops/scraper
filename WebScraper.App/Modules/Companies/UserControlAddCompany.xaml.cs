﻿using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebScraper.App.Modules.Settings;

namespace WebScraper.App.Modules.Companies
{
    /// <summary>
    /// Interaction logic for UserControlAddCompany.xaml
    /// </summary>
    public partial class UserControlAddCompany : UserControl
    {
        private readonly IMediator _mediator;
        Stopwatch sw = new Stopwatch();
        WebClient webClient = new WebClient();
        public UserControlAddCompany(IMediator mediator)
        {
            _mediator = mediator;
            InitializeComponent();
            DataContext = new Create.Command();
        }

        private async void AddCompanyButton_Click(object sender, RoutedEventArgs e)
        {
            AddCompanyButton.IsEnabled = false;
            Progress.Visibility = Visibility.Visible;
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            mainWindow.ShowLoadingIcon();
            mainWindow.HideMenu();

            var htmlList = await RunDownloadAsync();
            await _mediator.Send(new Create.Command() { Name = Name.Text, HtmlList = htmlList, Url1 = Url1.Text, Url2 = Url2.Text, Url3 = Url3.Text, Url4 = Url4.Text, Url5 = Url5.Text, Url6 = Url6.Text, Url7 = Url7.Text });

            mainWindow.HideLoadingIcon();
            mainWindow.ShowMenu();
            AddCompanyButton.IsEnabled = true;
            Header.Text = "Zakończono";

        }
        private async Task<List<string>> RunDownloadAsync()
        {
            var output = new List<string>();
            var one = await DownloadWebsiteAsync(Url1.Text);
            var two = await DownloadWebsiteAsync(Url2.Text);
            var three = await DownloadWebsiteAsync(Url3.Text);
            var four = await DownloadWebsiteAsync(Url4.Text);
            var five = await DownloadWebsiteAsync(Url5.Text);
            var six = await DownloadWebsiteAsync(Url6.Text);
            var seven = await DownloadWebsiteAsync(Url7.Text);


            if (one != null)
            {
                output.Add(one);
            }
            if (two != null)
            {
                output.Add(two);
            }
            if (three != null)
            {
                output.Add(three);
            }
            if (four != null)
            {
                output.Add(four);
            }
            if (five != null)
            {
                output.Add(five);
            }
            if (six != null)
            {
                output.Add(six);
            }
            if (seven != null)
            {
                output.Add(seven);
            }

            return output;

        }

        private async Task<string> DownloadWebsiteAsync(string url)
        {
            string output = null;
            var settings = await _mediator.Send(new GetLast.Query());
            string firstCookieString = null;
            string secondCookieString = null;

            if (String.IsNullOrEmpty(settings.Language) || String.IsNullOrEmpty(settings.Currency) || String.IsNullOrEmpty(settings.Country))
            {
                firstCookieString = "ih-preference=store=0&country=PL&language=en-US&currency=USD;";
                secondCookieString = "iher-pref1=storeid=0&sccode=PL&lan=en-US&scurcode=USD&wp=1&lchg=1&ifv=1&bi=0";
            }
            else
            {
                firstCookieString = $"ih-preference=store=0&country={settings.Country}&language={settings.Language}&currency={settings.Currency};";
                secondCookieString = $"iher-pref1=storeid=0&sccode={settings.Country}&lan={settings.Language}&scurcode={settings.Currency}&wp=1&lchg=1&ifv=1&bi=0";
            }


            if (!String.IsNullOrEmpty(url))
            {
                using (webClient = new WebClient())
                {
                    webClient.Encoding = Encoding.UTF8;
                    webClient.Headers.Add(HttpRequestHeader.UserAgent, "");

                    webClient.Headers.Add(HttpRequestHeader.Cookie, firstCookieString + secondCookieString);
                    webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);

                    // Start the stopwatch which we will be using to calculate the download speed
                    sw.Start();

                    try
                    {
                        output = await webClient.DownloadStringTaskAsync(url);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
           
            return output;
        }
        // The event that will fire whenever the progress of the WebClient is changed
        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            // Calculate download speed and output it to labelSpeed.
            Speed.Content = string.Format("{0} kb/s", (e.BytesReceived / 1024d / sw.Elapsed.TotalSeconds).ToString("0.00"));

            // Update the progressbar percentage only when the value is not the same.
            Progress.Visibility = Visibility.Visible;
            Progress.Value = e.ProgressPercentage;

            // Show the percentage on our label.
            Perc.Content = e.ProgressPercentage.ToString() + "%";

            // Update the label with how much data have been downloaded so far and the total size of the file we are currently downloading
            Downloaded.Content = string.Format("{0} MB's / {1} MB's",
                (e.BytesReceived / 1024d / 1024d).ToString("0.00"),
                (e.TotalBytesToReceive / 1024d / 1024d).ToString("0.00"));
        }

        private void goBackButton_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            var companiesList = new UserControlCompaniesList(_mediator);
            mainWindow.GridMain.Children.Clear();
            mainWindow.GridMain.Children.Add(companiesList);
        }


    }
}
