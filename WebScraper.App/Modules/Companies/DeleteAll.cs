﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Core.Domain.Companies;
using WebScraper.Core.Domain.Products;
using WebScraper.Core.Services;
using WebScraper.Infra.Data;


namespace WebScraper.App.Modules.Companies
{
    public static class DeleteAll
    {
        public class Command : IRequest
        {          
        }
       
        public class Handler : IRequestHandler<Command>
        {
            private readonly AppDb _db;
            public Handler(AppDb db)
            {
                _db = db;
            }

            public async Task<Unit> Handle(Command command, CancellationToken token)
            {
                var companies = await _db.Companies.ToListAsync(token);
                foreach (var company in companies)
                {
                    string companyDirectory = $@"Images\{company.Name}";

                    if (Directory.Exists(companyDirectory))
                    {
                        Directory.Delete(companyDirectory, true);
                    }

                    _db.Companies.Remove(company);
                    _db.SaveChanges();
                }
                return Unit.Value;

            }

        }
    }
}
