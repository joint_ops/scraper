﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Companies
{
    public static class DeleteCompany
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly AppDb _db;
            public Handler(AppDb db)
            {
                _db = db;
            }

            public async Task<Unit> Handle(Command command, CancellationToken token)
            {
                var company = await _db.Companies.FirstOrDefaultAsync(c => c.Id == command.Id, token);
                if (company == null)
                    throw new Exception($"cannot find company with Id:{command.Id}");

                string companyDirectory = $@"Images\{company.Name}";

                if (Directory.Exists(companyDirectory))
                {
                    Directory.Delete(companyDirectory, true);
                }

                _db.Companies.Remove(company);
                await _db.SaveChangesAsync(token);
                return Unit.Value;

            }

        }
    }
}
