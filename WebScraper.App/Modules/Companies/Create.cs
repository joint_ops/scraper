﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using WebScraper.Core.Domain.Companies;
using WebScraper.Core.Domain.Products;
using WebScraper.Core.Services;
using WebScraper.Infra.Data;


namespace WebScraper.App.Modules.Companies
{
    public static class Create
    {
        public class Command : IRequest<string>, IDataErrorInfo
        {
            private readonly CommandValidator _commandValidator;

            public string Name { get; set; }         
            public List<string> HtmlList { get; set; }
            public string Url1 { get; set; }
            public string Url2 { get; set; }
            public string Url3 { get; set; }
            public string Url4 { get; set; }
            public string Url5 { get; set; }
            public string Url6 { get; set; }
            public string Url7 { get; set; }

            public Command()
            {
                _commandValidator = new CommandValidator();
            }
            public string this[string columnName]
            {
                get
                {
                    var firstOrDefault = _commandValidator.Validate(this).Errors.FirstOrDefault(lol => lol.PropertyName == columnName);
                    if (firstOrDefault != null)
                        return _commandValidator != null ? firstOrDefault.ErrorMessage : "";
                    return "";
                }
            }
            public string Error
            {
                get
                {
                    if (_commandValidator != null)
                    {
                        var results = _commandValidator.Validate(this);
                        if (results != null && results.Errors.Any())
                        {
                            var errors = string.Join(Environment.NewLine, results.Errors.Select(x => x.ErrorMessage).ToArray());
                            return errors;
                        }
                    }
                    return string.Empty;
                }
            }
          
           
        }      
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(c => c.Name)
                    .Cascade(CascadeMode.StopOnFirstFailure)
                    .NotEmpty()
                    .Must(Name =>
                    {
                        using (var db = new AppDb())
                        {
                            return !db.Companies.Any(c => c.Name == Name);
                        }
                    })
                    .WithName("Nazwa firmy")
                .WithMessage("Ta firma juz istnieje.");


                RuleFor(c => c.Url1)
                    .Cascade(CascadeMode.StopOnFirstFailure)
                    .NotEmpty()                 
                    .Must(IsIHerbUrl).WithMessage("Link musi pochodzić z domeny : https://pl.iherb.com")
                    .Must(HasMaxProducts).WithMessage("Link musi zawierać maksymalną ilość produktów (192)")
                    .Must(Url1 =>
                    {
                        using (var db = new AppDb())
                        {
                            return !db.Pages.Any(c => c.Url == Url1);
                        }
                    })
                    .WithName("Pierwszy link")
                .WithMessage("Ta strona już istnieje.");

                RuleFor(c => c.Url2)
                    .Cascade(CascadeMode.StopOnFirstFailure)
                    .Must(IsIHerbUrl).WithMessage("Link musi pochodzić z domeny : https://pl.iherb.com")
                    .Must(HasMaxProducts).WithMessage("Link musi zawierać maksymalną ilość produktów (192)")
                    .Must(IsPage2).WithMessage("Link musi być stroną drugą (p=2)")
                    .Must(Url2 =>
                    {
                        using (var db = new AppDb())
                        {
                            return !db.Pages.Any(c => c.Url == Url2);
                        }
                    })
                    .WithName("Drugi link")
                .WithMessage("Ta strona już istnieje.");
                

                RuleFor(c => c.Url3)
                    .Cascade(CascadeMode.StopOnFirstFailure)
                    .Must(IsIHerbUrl).WithMessage("Link musi pochodzić z domeny : https://pl.iherb.com")
                    .Must(HasMaxProducts).WithMessage("Link musi zawierać maksymalną ilość produktów (192)")
                    .Must(IsPage3).WithMessage("Link musi być stroną trzecią (p=3)")
                    .Must(Url3 =>
                    {
                         using (var db = new AppDb())
                         {
                             return !db.Pages.Any(c => c.Url == Url3);
                         }
                    })
                     .WithName("Trzeci link")
                .WithMessage("Ta strona już istnieje.");

                RuleFor(c => c.Url4)
                    .Cascade(CascadeMode.StopOnFirstFailure)
                    .Must(IsIHerbUrl).WithMessage("Link musi pochodzić z domeny : https://pl.iherb.com")
                    .Must(HasMaxProducts).WithMessage("Link musi zawierać maksymalną ilość produktów (192)")
                    .Must(IsPage4).WithMessage("Link musi być stroną czwartą (p=4)")
                    .Must(Url4 =>
                    {
                        using (var db = new AppDb())
                        {
                            return !db.Pages.Any(c => c.Url == Url4);
                        }
                    })
                     .WithName("Czwarty link")
                .WithMessage("Ta strona już istnieje.");

                RuleFor(c => c.Url5)
                    .Cascade(CascadeMode.StopOnFirstFailure)                                     
                    .Must(IsIHerbUrl).WithMessage("Link musi pochodzić z domeny : https://pl.iherb.com")
                    .Must(HasMaxProducts).WithMessage("Link musi zawierać maksymalną ilość produktów (192)")
                    .Must(IsPage5).WithMessage("Link musi być stroną piątą (p=5)")
                    .Must(Url5 =>
                    {
                        using (var db = new AppDb())
                        {
                            return !db.Pages.Any(c => c.Url == Url5);
                        }
                    })
                     .WithName("Piąty link")
                .WithMessage("Ta strona już istnieje.");

                RuleFor(c => c.Url6)
                   .Cascade(CascadeMode.StopOnFirstFailure)
                   .Must(IsIHerbUrl).WithMessage("Link musi pochodzić z domeny : https://pl.iherb.com")
                   .Must(HasMaxProducts).WithMessage("Link musi zawierać maksymalną ilość produktów (192)")
                   .Must(IsPage6).WithMessage("Link musi być stroną szóstą (p=6)")
                   .Must(Url6 =>
                   {
                       using (var db = new AppDb())
                       {
                           return !db.Pages.Any(c => c.Url == Url6);
                       }
                   })
                    .WithName("Szósty link")
               .WithMessage("Ta strona już istnieje.");

                RuleFor(c => c.Url7)
                   .Cascade(CascadeMode.StopOnFirstFailure)
                   .Must(IsIHerbUrl).WithMessage("Link musi pochodzić z domeny : https://pl.iherb.com")
                   .Must(HasMaxProducts).WithMessage("Link musi zawierać maksymalną ilość produktów (192)")
                   .Must(IsPage7).WithMessage("Link musi być stroną siódmą (p=7)")
                   .Must(Url7 =>
                   {
                       using (var db = new AppDb())
                       {
                           return !db.Pages.Any(c => c.Url == Url7);
                       }
                   })
                    .WithName("Siódmy link")
               .WithMessage("Ta strona już istnieje.");

            }
                     
           private bool IsIHerbUrl(Command command,string url)
           {
                if (!String.IsNullOrEmpty(url))
                {
                    if (url.StartsWith("http://pl.iherb.com") || url.StartsWith("https://pl.iherb.com"))
                    {
                        return true;
                    }
                    return false;
                }
                return true;                
           }
            private bool IsPage2(Command command, string url)
            {
                if (!String.IsNullOrEmpty(url))
                {
                    if (url.EndsWith("p=2"))
                    {
                        return true;
                    }
                    return false;
                }
                return true;
            }
            private bool IsPage3(Command command, string url)
            {
                if (!String.IsNullOrEmpty(url))
                {
                    if (url.EndsWith("p=3"))
                    {
                        return true;
                    }
                    return false;
                }
                return true;
            }
            private bool IsPage4(Command command, string url)
            {
                if (!String.IsNullOrEmpty(url))
                {
                    if (url.EndsWith("p=4"))
                    {
                        return true;
                    }
                    return false;
                }
                return true;
            }
            private bool IsPage5(Command command, string url)
            {
                if (!String.IsNullOrEmpty(url))
                {
                    if (url.EndsWith("p=5"))
                    {
                        return true;
                    }
                    return false;
                }
                return true;
            }
            private bool IsPage6(Command command, string url)
            {
                if (!String.IsNullOrEmpty(url))
                {
                    if (url.EndsWith("p=6"))
                    {
                        return true;
                    }
                    return false;
                }
                return true;
            }
            private bool IsPage7(Command command, string url)
            {
                if (!String.IsNullOrEmpty(url))
                {
                    if (url.EndsWith("p=7"))
                    {
                        return true;
                    }
                    return false;
                }
                return true;
            }
            private bool HasMaxProducts(Command command, string url)
            {
                if (!String.IsNullOrEmpty(url))
                {
                    if (url.Contains("192"))
                    {
                        return true;
                    }
                    return false;
                }
                return true;
            }
           

        }       
        public class Handler : IRequestHandler<Command,string>
        {
            private readonly AppDb _db;
            public Handler(AppDb db)
            {
                _db = db;
            }
             
            public async Task<string> Handle(Command command, CancellationToken token)
            {                
                var companyId = Guid.NewGuid();
                var newCompany = new Company(companyId,command.Name);


                if (!String.IsNullOrWhiteSpace(command.Url1) && !String.IsNullOrWhiteSpace(command.HtmlList[0]))
                {
                    _db.Pages.Add(new Page(companyId, command.HtmlList[0], command.Url1));
                }
                if (!String.IsNullOrWhiteSpace(command.Url2) && !String.IsNullOrWhiteSpace(command.HtmlList[1]))
                {
                    _db.Pages.Add(new Page(companyId, command.HtmlList[1], command.Url2));
                }
                if (!String.IsNullOrWhiteSpace(command.Url3) && !String.IsNullOrWhiteSpace(command.HtmlList[2]))
                {
                    _db.Pages.Add(new Page(companyId, command.HtmlList[2], command.Url3));
                }
                if (!String.IsNullOrWhiteSpace(command.Url4) && !String.IsNullOrWhiteSpace(command.HtmlList[3]))
                {
                    _db.Pages.Add(new Page(companyId, command.HtmlList[3], command.Url4));
                }
                if (!String.IsNullOrWhiteSpace(command.Url5) && !String.IsNullOrWhiteSpace(command.HtmlList[4]))
                {
                    _db.Pages.Add(new Page(companyId, command.HtmlList[4], command.Url5));
                }
                if (!String.IsNullOrWhiteSpace(command.Url6) && !String.IsNullOrWhiteSpace(command.HtmlList[5]))
                {
                    _db.Pages.Add(new Page(companyId, command.HtmlList[5], command.Url6));
                }
                if (!String.IsNullOrWhiteSpace(command.Url7) && !String.IsNullOrWhiteSpace(command.HtmlList[6]))
                {
                    _db.Pages.Add(new Page(companyId, command.HtmlList[6], command.Url7));
                }

                await _db.Companies.AddAsync(newCompany, token);
                await _db.CommitTransactionAsync();

                return newCompany.Name;

            }
           

        }
    }
}
