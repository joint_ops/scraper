﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Infra.Data;
using WebScraper.Core.Services;

namespace WebScraper.App.Modules.Products
{
    public static class GetProductLinksByCompanyId
    {
        public class Query : IRequest<Result>
        {
            public Guid CompanyId { get; set; }
            
        }
        public class Result
        {
            public Guid CompanyId { get; set; }
            public string Name { get; set; }
            public List<string> Links { get; set; }
           
        }


        public class Handler : IRequestHandler<Query, Result>
        {
            private readonly AppDb _db;
            private readonly IMapper _mapper;
            private readonly IScraperService _scraperService;

            public Handler(AppDb db, IMapper mapper, IScraperService scraperService)
            {
                _db = db;
                _mapper = mapper;
                _scraperService = scraperService;
            }

            public async Task<Result> Handle(Query query, CancellationToken token)
            {
                var pages = await _db.Pages
                    .AsNoTracking()
                    .OrderBy(p => p.Id)
                    .Where(p => p.CompanyId == query.CompanyId)
                    .Include(p =>p.Company)
                    .ToListAsync(token);

                var links = new List<string>();

                foreach (var page in pages)
                {
                    foreach (var link in _scraperService.GetProductsUrls(page.Html))
                    {
                        links.Add(link);
                    }
                }

                return new Result() { Links = links,CompanyId = query.CompanyId ,Name = pages[0].Company.Name};
            }

           

        }
    }
}
