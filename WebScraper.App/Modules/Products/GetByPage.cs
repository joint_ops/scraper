﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Infra.Data;
using X.PagedList;
using WebScraper.Core.Domain.Products;
using WebScraper.Core.Domain.Companies;
using System.Linq;

namespace WebScraper.App.Modules.Products
{
    public static class GetByPage
    {
        public class Query : IRequest<Result>
        {
            public Guid CompanyId { get; set; }
            public int? Items { get; set; }
            public int? Page { get; set; }
        }
       
        public class Result
        {
            public int ItemsPerPage { get; set; }
            public int PageNumber { get; set; }
            public int NextPage { get; set; }
            public int PreviousPage { get; set; }
            public int TotalItems { get; set; }
            public bool HasNextPage { get; set; }
            public bool HasPreviousPage { get; set; }
            public IEnumerable<Product> Products { get; set; }

            public Result()
            {
                Products = new List<Product>();
            }

            public class Product
            {
                public Guid Id { get; set; }
                public string Name { get; set; }
                public string Url { get; set; }
                public string StockStatus { get; set; }
                public decimal Price { get; set; }
                public string Currency { get; set; }
                public string Quantity { get; set; }
                public string UpcCode { get; set; }
                public string ProductCode { get; set; }
                public string Description { get; set; }
                public string Dimensions { get; set; }
                public string Weight { get; set; }
                public string FirstCategory { get; set; }
                public string SecondCategory { get; set; }
                public string Brand { get; set; }
                public DateTime? ExpirationDate { get; set; }
                public DateTime CreatedAt { get; set; }
                public DateTime? UpdatedAt { get; set; }
                public Company Company { get; set; }

            }
            public class Company
            {
                public string Name { get; set; }
            }

        }
        public class Handler : IRequestHandler<Query, Result>
        {
            private readonly AppDb _db;
            private readonly IMapper _mapper;
            private int _previousPage;
            private int _nextPage;

            public Handler(AppDb db, IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }

            public async Task<Result> Handle(Query query, CancellationToken token)
            {

                var products = await _db.Products
                    .AsNoTracking()
                    .Where( p => p.CompanyId == query.CompanyId)
                    .ProjectTo<Result.Product>(_mapper.ConfigurationProvider)
                    .ToPagedListAsync(query.Page ?? 1, query.Items ?? 24, token);


                foreach (var product in products)
                {
                    if (product.Brand != null)
                    {

                        product.Name = product.Name.Replace($"{product.Brand},", "");
                    }

                }
               
                if (query.Page > 1)
                    _previousPage = MakePreviousPage(query.Items, query.Page);

                if ((query.Page ?? 1) <= (products.TotalItemCount / (query.Items ?? 24)))
                    _nextPage = MakeNextPage(query.Items, query.Page);

                return new Result()
                {
                    ItemsPerPage = query.Items ?? 24,
                    PageNumber = query.Page ?? 1,
                    NextPage = _nextPage,
                    PreviousPage = _previousPage,
                    TotalItems = products.TotalItemCount,
                    HasNextPage = products.HasNextPage,
                    HasPreviousPage = products.HasPreviousPage,                  
                    Products = products
                };
            }
            public class MappingProfile : Profile
            {
                public MappingProfile()
                {
                    CreateMap<Product, Result.Product>();
                    CreateMap<Company, Result.Company>();
                }
            }
            private static int MakeNextPage(int? items, int? page)
            {
                return  (page ?? 1) + 1 ;
            }
            private static int MakePreviousPage(int? items, int? page)
            {
                return (page ?? 1) - 1 ;
            }
        }

    }
}
