﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using WebScraper.Core.Domain.Products;
using WebScraper.Core.Services;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Products
{
    public static class CreateTableImageById
    {
        public class Command : IRequest<string>
        {
            public Guid ProductId { get; set; }
        }
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {

            }
        }

        public class Handler : IRequestHandler<Command, string>
        {
            private readonly AppDb _db;
            private readonly IHtmlToImageService _htmlToImageService;

            public Handler(AppDb db, IHtmlToImageService htmlToImageService)
            {
                _db = db;
                _htmlToImageService = htmlToImageService; 
            }

            public async Task<string> Handle(Command command, CancellationToken token)
            {
                var product = await _db.Products
                    .AsNoTracking()
                    .Include(p => p.Company)
                    .FirstOrDefaultAsync(p => p.Id == command.ProductId,token);

                var setting = await _db.Settings
                    .AsNoTracking()
                    .FirstOrDefaultAsync(token);

                if (!Directory.Exists(setting.ImagesPath))
                {
                    Directory.CreateDirectory(setting.ImagesPath);
                }
                var companyDirectory = $@"{setting.ImagesPath}\Images\{product.Company.Name}";


                if (!Directory.Exists(companyDirectory))
                {
                    Directory.CreateDirectory(companyDirectory);
                }

               


                var path = $@"{companyDirectory}\{product.ProductCode}.jpg";

                if (product == null)
                    throw new Exception("Product is null");

                if (product.SupplementTable == null)
                {                   
                    return $"{product.Name} nie posiada tabeli.";
                }
                await Task.Run( () =>
                {
                    _htmlToImageService.CreateImage(product.SupplementTable, path);
                });
                return product.Name;
            }

        }
    }
}


