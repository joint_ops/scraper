﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using WebScraper.Core.Domain.Products;
using WebScraper.Core.Services;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Products
{
    public static class AddCompanyProducts
    {
        public class Command : IRequest<int>
        {
            public Guid CompanyId { get; set; }
            public List<string> HtmlList { get; set; }
        }
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {

            }
        }
      
        public class Handler : IRequestHandler<Command,int>
        {
            private readonly AppDb _db;
            private readonly IScraperService _scraperService;

            public Handler(AppDb db, IScraperService scraperService)
            {
                _db = db;
                _scraperService = scraperService;
            }

            public async Task<int> Handle(Command command, CancellationToken token)
            {
                var company = await _db.Companies
                    .Include(c =>c.Products)
                    .FirstOrDefaultAsync(c => c.Id == command.CompanyId, token);

                var setting = await _db.Settings.AsNoTracking().FirstOrDefaultAsync(token);

                var sanitizedHtml = new List<string>();

                foreach (var html in command.HtmlList)
                {
                    if (!string.IsNullOrEmpty(html))
                    {
                        sanitizedHtml.Add(_scraperService.Sanitize(html));
                    }
                   
                }

                await Task.Run(async () =>
                {
                    if (company.Products.Count > 0)
                    {
                        _db.Products.RemoveRange(company.Products);
                    }
                  
                    foreach (var html in sanitizedHtml)
                    {
                        var id = Guid.NewGuid();

                        _db.Products.Add(new Product(
                            id,
                            command.CompanyId,
                            _scraperService.GetSingleProductName(html),
                            _scraperService.GetUrl(html),
                            _scraperService.GetStockStatus(html),
                            _scraperService.GetPrice(html),
                            _scraperService.GetCurrency(html),
                            _scraperService.GetQuantity(html),
                            _scraperService.GetUpcCode(html),
                            _scraperService.GetProductCode(html),
                            _scraperService.GetSuplementTable(html),
                            _scraperService.GetDimensions(html),
                            _scraperService.GetDescription(html),
                            _scraperService.GetDescriptionList(html),
                            _scraperService.GetIngredients(html),
                            _scraperService.GetSuggestedUse(html),
                            _scraperService.GetShippingWeight(html),
                            _scraperService.Get1stCategory(html),
                            _scraperService.Get2ndCategory(html),
                            _scraperService.GetBrand(html),
                            _scraperService.GetExpirationDate(html),
                            true
                            )); ;

                        var imageLinks = _scraperService.GetImages(html);
                       
                        for (int i = 0; i < imageLinks.Count; i++)
                        {
                            var productCode = _scraperService.GetProductCode(html);
                            _db.ProductImages.Add(new ProductImage(id, imageLinks[i],@$"{company.Name}\{productCode}{i}.jpg"));
                        }
                    }
                    company.SetHasCompletedProducts(true);
                    company.SetHasCompletedImages(true);
                    await _db.CommitTransactionAsync();

                });
                
                return sanitizedHtml.Count;

            }

        }
    }
}
