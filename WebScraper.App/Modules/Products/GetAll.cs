﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Core.Domain.Products;
using WebScraper.Core.Domain.Companies;
using WebScraper.Core.Services;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Products
{
    public static class GetAll
    {
        public class Query : IRequest<Result>
        {

        }
        public class Result
        {
            public List<Product> Products { get; set; }
            public Result()
            {
                Products = new List<Product>();
            }

            public class Product
            {
                public Guid Id { get; set; }
                public string Name { get; set; }
                public string Url { get; set; }
                public string StockStatus { get; set; }
                public decimal Price { get; set; }
                public string Currency { get; set; }
                public string Quantity { get; set; }
                public string UpcCode { get; set; }
                public string ProductCode { get; set; }
                public string Description { get; set; }
                public string Dimensions { get; set; }
                public string Weight { get; set; }
                public DateTime? ExpirationDate { get; set; }
                public DateTime CreatedAt { get; set; }
                public DateTime? UpdatedAt { get; set; }
                public Company Company { get; set; }


            }
            public class Company
            {
                public Guid Id { get; set; }
                public string Name { get; set; }
            }

        }


        public class Handler : IRequestHandler<Query, Result>
        {
            private readonly AppDb _db;
            private readonly IMapper _mapper;

            public Handler(AppDb db, IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }

            public async Task<Result> Handle(Query query, CancellationToken token)
            {
                var products = await _db.Products
                    .AsNoTracking()
                    .OrderBy(p => p.Name)
                    .Include(p => p.Company)
                    .ProjectTo<Result.Product>(_mapper.ConfigurationProvider)
                    .ToListAsync(token);

                foreach (var product in products)
                {
                    string text = $"{product.Company.Name},";
                    product.Name = product.Name.Replace(text, "");
                }

                return new Result() { Products = products };
            }

            public class MappingProfile : Profile
            {
                public MappingProfile()
                {
                    CreateMap<Product, Result.Product>();
                    CreateMap<Company, Result.Company>();
                }
            }

        }
    }
}
