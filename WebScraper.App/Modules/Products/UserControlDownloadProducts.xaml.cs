﻿using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebScraper.App.Modules.Companies;
using WebScraper.App.Modules.Images;
using WebScraper.App.Modules.Settings;
using WebScraper.Core.Services;

namespace WebScraper.App.Modules.Products
{
    /// <summary>
    /// Interaction logic for UserControlProduct.xaml
    /// </summary>
    
    public partial class UserControlDownloadProducts : UserControl
    {
        private readonly IMediator _mediator;
        CancellationTokenSource cts = new CancellationTokenSource();

        public UserControlDownloadProducts(IMediator mediator,Guid companyId)
        {
            _mediator = mediator;
            InitializeComponent();
            ExecuteAsync(companyId);         
        }
        private async Task<GetProductLinksByCompanyId.Result> GetProductLinks(Guid companyId)
        {
            return await _mediator.Send(new GetProductLinksByCompanyId.Query() { CompanyId = companyId });
        }
        private async void ExecuteAsync(Guid companyId)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            mainWindow.ShowLoadingIcon();
            mainWindow.HideMenu();
            var progress = new Progress<ProgressReportModel>();         
            progress.ProgressChanged += ReportDLProgress;
            var dlWatch = Stopwatch.StartNew();
            var result = await GetProductLinks(companyId);

            var links = new List<string>();
            ActionText.Text = $"Pobieranie produktów firmy {result.Name}";
            try
            {
                links = await RunDownloadAsync(result, progress,cts.Token);
                dlWatch.Stop();
                Result.Content = $"Pobrano {links.Count()} produktów,czas wykonania: { dlWatch.Elapsed.TotalMinutes } minut.";


                ActionText.Text = "Zapisywanie produktów do bazy danych...";
                progress.ProgressChanged += ReportDBProgress;
                var dbWatch = Stopwatch.StartNew();
                var dbWrite = await UpdateProductHtml(result.CompanyId, links, progress);
                dbWatch.Stop();
                Result.Content = $"Zakończono,{dbWrite} produktów zapisanych ,czas wykonania: { dbWatch.Elapsed.TotalMinutes } minut.";

                ActionText.Text = "Zakończono.";
                mainWindow.HideLoadingIcon();
                mainWindow.ShowMenu();
            }
            catch (OperationCanceledException)
            {
                ActionText.Text = "Proces anulowano";
                Result.Content = $"Zakończono,proces anulowany przez użytkownika.";
                DownloadProgress.Value = 100;
                mainWindow.HideLoadingIcon();
                mainWindow.ShowMenu();
            }

            CancelButton.Visibility = Visibility.Hidden;
            GoBackButton.Visibility = Visibility.Visible;
        }
        private void goBackButton_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            var companiesList = new UserControlCompaniesList(_mediator);
            mainWindow.GridMain.Children.Clear();
            mainWindow.GridMain.Children.Add(companiesList);
        }
        private void cancelOperation_Click(object sender, RoutedEventArgs e)
        {
            cts.Cancel();
        }

        private void ReportDLProgress(object sender, ProgressReportModel e)
        {
            DownloadProgress.Value = e.PercentageComplete;
        }
        private void ReportDBProgress(object sender, ProgressReportModel e)
        {
            DownloadProgress.Value = e.PercentageComplete;
        }

        private async Task<int> UpdateProductHtml(Guid companyId,List<string> htmlList, IProgress<ProgressReportModel> progress)
        {

            return await _mediator.Send(new AddCompanyProducts.Command() { CompanyId = companyId, HtmlList = htmlList  });
        }
        private async Task<List<string>> RunDownloadAsync(GetProductLinksByCompanyId.Result data, IProgress<ProgressReportModel> progress, CancellationToken cancellationToken)
        {          
            var output = new List<string>();
            ProgressReportModel report = new ProgressReportModel();


            foreach (var link in data.Links)
            {
                var result = await DownloadWebsiteAsync(link);

                output.Add(result.Html);
                cancellationToken.ThrowIfCancellationRequested();
                Result.Content = $"Pobieranie {result.Url}";
                report.PercentageComplete = (output.Count * 100) / data.Links.Count;
                progress.Report(report);
            }

            return output;
        }
        private async Task<Dldata> DownloadWebsiteAsync(string url)
        {
            var settings = await _mediator.Send(new GetLast.Query());
            string firstCookieString = null;
            string secondCookieString = null;

            if (String.IsNullOrEmpty(settings.Language) || String.IsNullOrEmpty(settings.Currency) || String.IsNullOrEmpty(settings.Country))
            {
                firstCookieString = "ih-preference=store=0&country=PL&language=en-US&currency=USD;";
                secondCookieString = "iher-pref1=storeid=0&sccode=PL&lan=en-US&scurcode=USD&wp=1&lchg=1&ifv=1&bi=0";
            }
            else
            {
                firstCookieString = $"ih-preference=store=0&country={settings.Country}&language={settings.Language}&currency={settings.Currency};";
                secondCookieString = $"iher-pref1=storeid=0&sccode={settings.Country}&lan={settings.Language}&scurcode={settings.Currency}&wp=1&lchg=1&ifv=1&bi=0";
            }

            var client = new WebClient();
            client.Encoding = Encoding.UTF8;
            //  client.Headers.Add(HttpRequestHeader.Cookie, "ih-preference=store=0&country=PL&language=en-US&currency=USD;" + "iher-pref1=storeid=0&sccode=PL&lan=en-US&scurcode=USD&wp=1&lchg=1&ifv=1&bi=0");
            client.Headers.Add(HttpRequestHeader.Cookie, firstCookieString + secondCookieString);
            var data = new Dldata();
            data.Url = url;
          
            try
            {              
                data.Html = await client.DownloadStringTaskAsync(url);
            }
            catch (Exception ex)
            {
                Errors.Text += $"{Environment.NewLine}{ex.Message} {Environment.NewLine}{url} {Environment.NewLine}";
                ErrorCount.Content = int.Parse(ErrorCount.Content.ToString()) + 1;
            }

            return data;
        }
      
        private class Dldata
        {
            public string Html { get; set; }
            public string Url { get; set; }
        }
       
        private class ProgressReportModel
        {
            public int PercentageComplete { get; set; } = 0;
        }
    }  

}
