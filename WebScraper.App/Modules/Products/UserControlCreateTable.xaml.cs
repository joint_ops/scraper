﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebScraper.App.Modules.Companies;

namespace WebScraper.App.Modules.Products
{
    /// <summary>
    /// Interaction logic for UserControlCreateTable.xaml
    /// </summary>
    public partial class UserControlCreateTable : UserControl
    {
        private readonly IMediator _mediator;
        CancellationTokenSource cts = new CancellationTokenSource();
        public UserControlCreateTable(IMediator mediator, Guid companyId)
        {
            _mediator = mediator;           
            InitializeComponent();
            CreateTables(companyId);
        }
        private void cancelOperation_Click(object sender, RoutedEventArgs e)
        {
            cts.Cancel();
        }
        async void CreateTables(Guid companyId)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            mainWindow.ShowLoadingIcon();
            mainWindow.HideMenu();

            var progress = new Progress<ProgressReportModel>();
            var result = await  _mediator.Send(new GetAllByCompanyId.Query() { CompanyId = companyId });
            ActionText.Text = $"Tworzenie tabel produktów firmy {result.Products[0].Company.Name}";
            progress.ProgressChanged += ReportTableProgress;
            var tableWatch = Stopwatch.StartNew();
            try
            {
                var tables = await CreateTablesAsync(result.Products, progress,cts.Token);
                tableWatch.Stop();
                Result.Content = $"Zakończono,{tables.Count} tabel zapisanych na dysk,czas wykonania: { tableWatch.Elapsed.TotalMinutes } minut.";
                ActionText.Text = "Zakończono.";
                mainWindow.HideLoadingIcon();
                mainWindow.ShowMenu();
            }
            catch (Exception)
            {
                ActionText.Text = "Proces anulowano";
                Result.Content = $"Zakończono,proces anulowany przez użytkownika.";
                DownloadProgress.Value = 100;
                mainWindow.HideLoadingIcon();
                mainWindow.ShowMenu();
            }

            CancelButton.Visibility = Visibility.Hidden;
            GoBackButton.Visibility = Visibility.Visible;

        }
        private void goBackButton_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            var companiesList = new UserControlCompaniesList(_mediator);
            mainWindow.GridMain.Children.Clear();
            mainWindow.GridMain.Children.Add(companiesList);
        }
        private async Task<List<string>> CreateTablesAsync(List<GetAllByCompanyId.Result.Product> products, IProgress<ProgressReportModel> progress, CancellationToken cancellationToken)
        {
            ProgressReportModel report = new ProgressReportModel();
            var output = new List<string>();

            for (int i = 0; i < products.Count; i++)
            {
                var name = await _mediator.Send(new CreateTableImageById.Command() { ProductId = products[i].Id });
                output.Add(name);
                cancellationToken.ThrowIfCancellationRequested();
                Result.Content = $"Tworzenie tabeli {name}";
                report.PercentageComplete = (output.Count * 100) / products.Count;
                progress.Report(report);
            }
            return output;
        }

        private void ReportTableProgress(object sender, ProgressReportModel e)
        {
            DownloadProgress.Value = e.PercentageComplete;
        }        
        private class ProgressReportModel
        {
            public int PercentageComplete { get; set; } = 0;
        }
    }
   
}
