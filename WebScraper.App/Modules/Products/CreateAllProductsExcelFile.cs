﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Core.Domain.Companies;
using WebScraper.Core.Domain.Products;
using WebScraper.Core.Helpers;
using WebScraper.Core.Services;
using WebScraper.Infra.Data;


namespace WebScraper.App.Modules.Products
{
    public static class CreateAllProductsExcelFile
    {
        public class Command : IRequest<string>
        {
        }
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {

            }
        }
        public class Result
        {
            public List<Product> Products { get; set; }

            public Result()
            {
                Products = new List<Product>();
            }

            public class Product
            {
                public Guid Id { get; set; }
                public string Name { get; set; }
                public string Url { get; set; }
                public string StockStatus { get; set; }
                public decimal Price { get; set; }
                public string Currency { get; set; }
                public string Quantity { get; set; }
                public string UpcCode { get; set; }
                public string ProductCode { get; set; }
                public string Dimensions { get; set; }
                public string Weight { get; set; }
                public DateTime? ExpirationDate { get; set; }
                public string DescriptionList { get; set; }
                public string Ingredients { get; set; }
                public string SuggestedUse { get; set; }
                public string FirstCategory { get; set; }
                public string SecondCategory { get; set; }
                public string Brand { get; set; }
                public Company Company { get; set; }
               
            }
            public class Company
            {
                public string Name { get; set; }
            }
        }
        public class Handler : IRequestHandler<Command, string>
        {
            private readonly AppDb _db;
            private readonly IMapper _mapper;
            public Handler(AppDb db, IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }

            public async Task<string> Handle(Command command, CancellationToken token)
            {
                var products = await _db.Products
                    .AsNoTracking()
                    .OrderBy(p =>p.Name)
                    .ProjectTo<Result.Product>(_mapper.ConfigurationProvider)
                    .ToListAsync(token);

                var setting = await _db.Settings.AsNoTracking().FirstOrDefaultAsync(token);
                if (setting.XlsPath == null)
                {
                    setting.XlsPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                }

                string fileName = @$"{setting.XlsPath}\Excel Files\IHerb lista wszystkich produktów.xls";

                if (!Directory.Exists($@"{setting.XlsPath}\Excel Files"))
                {
                    Directory.CreateDirectory($@"{setting.XlsPath}\Excel Files");
                }

                var file = File.Create(fileName);

                // create an Excel file objects 
                XSSFWorkbook workbook = new XSSFWorkbook();
                // add a Sheet 
                ISheet sheet = workbook.CreateSheet("IHerb lista wszystkich produktów");

                // data is gradually written sheet each row 
                if (products.Count > 0)
                {
                    for (int i = 0; i < products.Count; i++)
                    {
                        IRow rowtemp = sheet.CreateRow(i);

                        rowtemp.CreateCell(0).SetCellValue(products[i].Name);
                        rowtemp.CreateCell(1).SetCellValue(products[i].Quantity);
                        rowtemp.CreateCell(2).SetCellValue(products[i].StockStatus);
                        rowtemp.CreateCell(3).SetCellValue(Decimal.ToDouble(products[i].Price));
                        rowtemp.CreateCell(4).SetCellValue(products[i].Currency);
                        rowtemp.CreateCell(5).SetCellValue(products[i].UpcCode);
                        rowtemp.CreateCell(6).SetCellValue(products[i].ProductCode);
                        rowtemp.CreateCell(7).SetCellValue(products[i].Dimensions);
                        rowtemp.CreateCell(8).SetCellValue(products[i].Weight);
                        rowtemp.CreateCell(9).SetCellValue(products[i].ExpirationDate.ToString());
                        rowtemp.CreateCell(10).SetCellValue(products[i].Brand);
                        rowtemp.CreateCell(11).SetCellValue(products[i].FirstCategory);
                        rowtemp.CreateCell(12).SetCellValue(products[i].SecondCategory);
                        rowtemp.CreateCell(13).SetCellValue(products[i].DescriptionList);
                        rowtemp.CreateCell(14).SetCellValue(products[i].Ingredients);
                        rowtemp.CreateCell(15).SetCellValue(products[i].SuggestedUse);
                    }
                }

                workbook.Write(file);

                return fileName;

            }
            public class MappingProfile : Profile
            {
                public MappingProfile()
                {
                    CreateMap<Product, Result.Product>();
                    CreateMap<Company, Result.Company>();
                }
            }

        }
    }
}
