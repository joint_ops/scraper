﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebScraper.App.Modules.Companies;

namespace WebScraper.App.Modules.Products
{
    /// <summary>
    /// Interaction logic for UserControlCompanyProductsList.xaml
    /// </summary>
    public partial class UserControlCompanyProductsList : UserControl
    {
        private readonly IMediator _mediator;
        private readonly Guid _companyId;
       

        public UserControlCompanyProductsList(IMediator mediator, Guid companyId)
        {
            InitializeComponent();
            _companyId = companyId;
           _mediator = mediator;
          
            GetAllProductsByCompanyId(companyId);
        }
        private async void GetAllProductsByCompanyId(Guid companyId)
        {
            var result = await _mediator.Send(new GetByPage.Query() { CompanyId = companyId ,Items = 25,Page =1 });
            CurentPageLabel.Content = 1;
            LastPageLabel.Content = (result.TotalItems - 1) / result.ItemsPerPage + 1;
            DataContext = result.Products;
            InfoLabel.Content = $"strona {result.PageNumber} z {(result.TotalItems - 1) / result.ItemsPerPage + 1}";          
            PreviousPageButton.IsEnabled = false;
            if (!result.HasNextPage)
            {
                NextPageButton.IsEnabled = false;
            }

        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            Hyperlink link = (Hyperlink)e.OriginalSource;
            OpenUrl(link.NavigateUri.AbsoluteUri);
        }
        private void OpenUrl(string url)
        {
            try
            {               
                Process.Start(url);
            }
            catch
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    url = url.Replace("&", "^&");
                    Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    Process.Start("xdg-open", url);
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    Process.Start("open", url);
                }
                else
                {
                    throw;
                }
            }
        }
        private async void nextPageButton_Click(object sender, RoutedEventArgs e)
        {
            NextPageButton.IsEnabled = false;
            int currentPage = Int32.Parse(CurentPageLabel.Content.ToString());
            var result = await _mediator.Send(new GetByPage.Query() { CompanyId = _companyId, Page = currentPage + 1, Items = 25 });

            CurentPageLabel.Content = result.PageNumber;
            InfoLabel.Content = $"strona {result.PageNumber} z {(result.TotalItems - 1) / result.ItemsPerPage + 1}";
            
            PreviousPageButton.IsEnabled = true;
            DataContext = result.Products;
            if (!result.HasNextPage)
            {
                NextPageButton.IsEnabled = false;
            }
            else
            {
                NextPageButton.IsEnabled = true;
            }

        }
        private async void previousPageButton_Click(object sender, RoutedEventArgs e)
        {
            PreviousPageButton.IsEnabled = false;
            int currentPage = Int32.Parse(CurentPageLabel.Content.ToString());
            var result = await _mediator.Send(new GetByPage.Query() { CompanyId = _companyId, Page = currentPage -1, Items = 25 });

            CurentPageLabel.Content = result.PageNumber;
            InfoLabel.Content = $"strona {result.PageNumber} z {(result.TotalItems - 1) / result.ItemsPerPage + 1}";
           
            NextPageButton.IsEnabled = true;
            DataContext = result.Products;
            if (!result.HasPreviousPage)
            {
                PreviousPageButton.IsEnabled = false;
            }
            else
            {
                PreviousPageButton.IsEnabled = true;
            }
        }
        private void goBackButton_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            var companiesList = new UserControlCompaniesList(_mediator);
            mainWindow.GridMain.Children.Clear();
            mainWindow.GridMain.Children.Add(companiesList);
        }

    }
}
