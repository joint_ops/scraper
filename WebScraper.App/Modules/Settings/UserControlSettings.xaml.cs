﻿using MediatR;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebScraper.App.Modules.Companies;

namespace WebScraper.App.Modules.Settings
{
    /// <summary>
    /// Interaction logic for UserControlSettings.xaml
    /// </summary>
    public partial class UserControlSettings : UserControl
    {
        private readonly IMediator _mediator;
        public UserControlSettings(IMediator mediator)
        {
            _mediator = mediator;
            InitializeComponent();
            GetLastSettings();
            Header.Text = "Ustawienia";
        }
        void ImagesButton_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new CommonOpenFileDialog();
            dlg.Title = "Wybierz folder wyjsciowy";
            dlg.IsFolderPicker = true;
            //  dlg.InitialDirectory = currentDirectory;

            dlg.AddToMostRecentlyUsedList = false;
            dlg.AllowNonFileSystemItems = false;
            //  dlg.DefaultDirectory = currentDirectory;
            dlg.EnsureFileExists = true;
            dlg.EnsurePathExists = true;
            dlg.EnsureReadOnly = false;
            dlg.EnsureValidNames = true;
            dlg.Multiselect = false;
            dlg.ShowPlacesList = true;

            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                var folder = dlg.FileName;
                ImagesPath.Text = folder;
            }
        }
        void XlsButton_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new CommonOpenFileDialog();
            dlg.Title = "Wybierz folder wyjsciowy";
            dlg.IsFolderPicker = true;
            //  dlg.InitialDirectory = currentDirectory;

            dlg.AddToMostRecentlyUsedList = false;
            dlg.AllowNonFileSystemItems = false;
            //  dlg.DefaultDirectory = currentDirectory;
            dlg.EnsureFileExists = true;
            dlg.EnsurePathExists = true;
            dlg.EnsureReadOnly = false;
            dlg.EnsureValidNames = true;
            dlg.Multiselect = false;
            dlg.ShowPlacesList = true;

            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                var folder = dlg.FileName;
                XlsPath.Text = folder;
            }
        }
        async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            mainWindow.ShowLoadingIcon();
            mainWindow.HideMenu();
            SaveButton.IsEnabled = false;

            await  _mediator.Send(new Create.Command() { ImagesPath = ImagesPath.Text, XlsPath = XlsPath.Text ,Country = Country.Text,Language = Language.Text,Currency = Currency.Text });

            mainWindow.HideLoadingIcon();
            mainWindow.ShowMenu();
            SaveButton.IsEnabled = true;
            Header.Text = "Zapisano";
        }
        async void GetLastSettings()
        {
            var setting = await _mediator.Send(new GetLast.Query());
            if (setting != null)
            {
                ImagesPath.Text = setting.ImagesPath;
                XlsPath.Text = setting.XlsPath;
                Country.Text = setting.Country;
                Language.Text = setting.Language;
                Currency.Text = setting.Currency;
            }
           
        }
        private void goBackButton_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            var companiesList = new UserControlCompaniesList(_mediator);
            mainWindow.GridMain.Children.Clear();
            mainWindow.GridMain.Children.Add(companiesList);
        }
       
    }
}
