﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Core.Domain.Settings;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Settings
{
    public static class Create
    {
        public class Command : IRequest
        {
            public string ImagesPath { get; set; }
            public string XlsPath { get; set; }
            public string Currency { get; set; }
            public string Language { get; set; }
            public string Country { get; set; }
        }
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(c => c.ImagesPath)
                    .Cascade(CascadeMode.StopOnFirstFailure)
                    .NotEmpty()
                    .MinimumLength(5);

                RuleFor(c => c.XlsPath)
                    .Cascade(CascadeMode.StopOnFirstFailure)
                    .NotEmpty()
                    .MinimumLength(5);

            }
        }
        public class Handler : IRequestHandler<Command>
        {
            private readonly AppDb _db;
            public Handler(AppDb db)
            {
                _db = db;
            }

            public async Task<Unit> Handle(Command command, CancellationToken token)
            {
                var settings = await _db.Settings.ToListAsync(token);
                
                foreach (var item in settings)
                {
                    if (item != null)
                    {
                        _db.Settings.Remove(item);
                    }
                   
                }
                await _db.SaveChangesAsync(token);

                var newSetting = new Setting(command.ImagesPath, command.XlsPath,command.Country,command.Currency,command.Language);

                await _db.Settings.AddAsync(newSetting);
                await _db.SaveChangesAsync(token);

                return Unit.Value;

            }


        }
    }
}
