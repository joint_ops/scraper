﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Core.Domain.Settings;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Settings
{
    public static class GetLast
    {
        public class Query : IRequest<Result>
        {

        }
        public class Result
        {
            public string ImagesPath { get; set; }
            public string XlsPath { get; set; }
            public string Currency { get; set; }
            public string Language { get; set; }
            public string Country { get; set; }
        }
        public class Handler : IRequestHandler<Query, Result>
        {
            private readonly AppDb _db;
            private readonly IMapper _mapper;

            public Handler(AppDb db, IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }
            public async Task<Result> Handle(Query query, CancellationToken token)
            {
                var setting = await _db.Settings
                    .AsNoTracking()                
                    .ProjectTo<Result>(_mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(token);

                if (setting == null)
                {
                    return new Result() { ImagesPath = "", XlsPath = "",Country = "",Currency ="",Language ="" };
                }
             
                return new Result() 
                { 
                    ImagesPath = setting.ImagesPath ,
                    XlsPath = setting.XlsPath,
                    Country = setting.Country,
                    Currency = setting.Currency,
                    Language = setting.Language
                };
            }
            public class MappingProfile : Profile
            {
                public MappingProfile()
                {
                    CreateMap<Setting, Result>();
                }
            }

        }
    }
}
