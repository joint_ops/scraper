﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Settings
{
    public static class CheckHost
    {
        public class Query : IRequest<bool>
        {
            public string Name { get; set; }
        }
       
        public class Handler : IRequestHandler<Query,bool>
        {
            private readonly AppDb _db;
            public Handler(AppDb db)
            {
                _db = db;
            }

            public async Task<bool> Handle(Query query, CancellationToken token)
            {
                var host = await _db.Hosts.AsNoTracking().AnyAsync( h => h.Name == query.Name, token);

                if (host)
                {
                    return true;
                }

                return false;

            }


        }
    }
}
