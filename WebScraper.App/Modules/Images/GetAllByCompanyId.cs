﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebScraper.Core.Domain.Companies;
using WebScraper.Core.Domain.Products;
using WebScraper.Infra.Data;

namespace WebScraper.App.Modules.Images
{
    public static class GetAllByCompanyId
    {
        public class Query : IRequest<Result>
        {
            public Guid CompanyId { get; set; }
        }
        public class Result
        {
            public List<Image> Images { get; set; }
            public Result()
            {
                Images = new List<Image>();
            }
            public class Image
            {
                public Guid Id { get; set; }
                public Guid ProductId { get; set; }
                public string Path { get; set; }
                public string Url { get; set; }
                public Product Product { get; set; }
            }
            public class Product
            {
                public Guid Id { get; set; }
                public Guid CompanyId { get; set;}
                public Company Company { get; set; }
                public string Name { get; set; }
                public string ProductCode {get;set;}
                public List<Image> Images { get; set; }
                public Product()
                {
                    Images = new List<Image>();
                }
            }
            public class Company
            {
                public Guid Id { get; set; }
                public string Name { get; set; }
                public List<Product> Products { get; set; }
                public Company()
                {
                    Products = new List<Product>();
                }
            }
        }


        public class Handler : IRequestHandler<Query, Result>
        {
            private readonly AppDb _db;
            private readonly IMapper _mapper;

            public Handler(AppDb db, IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }

            public async Task<Result> Handle(Query query, CancellationToken token)
            {
                var images = await _db.ProductImages
                    .AsNoTracking()
                    .Include(p => p.Product)
                    .ThenInclude(p => p.Company)
                    .Where(p => p.Product.Company.Id == query.CompanyId)
                    .ProjectTo<Result.Image>(_mapper.ConfigurationProvider)
                    .ToListAsync(token);

                var setting = await _db.Settings.AsNoTracking().FirstOrDefaultAsync(token);
                if (setting.ImagesPath == null)
                {
                    setting.ImagesPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                }

                
                if (!Directory.Exists(setting.ImagesPath))
                {
                    Directory.CreateDirectory(setting.ImagesPath);
                }
                foreach (var image in images)
                {
                    image.Path = $@"{setting.ImagesPath}\Images\{image.Path}";
                    var companyDirectory = $@"{setting.ImagesPath}\Images\{image.Product.Company.Name}";
                    if (!Directory.Exists(companyDirectory))
                    {
                        Directory.CreateDirectory(companyDirectory);
                    }
                }

                return new Result() { Images = images };
            }

            public class MappingProfile : Profile
            {
                public MappingProfile()
                {
                    CreateMap<ProductImage, Result.Image>();
                    CreateMap<Product, Result.Product>();
                    CreateMap<Company, Result.Company>();
                }
            }

        }
    }
}


