﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebScraper.App.Modules.Companies;

namespace WebScraper.App.Modules.Images
{
    /// <summary>
    /// Interaction logic for UserControlDownloadImages.xaml
    /// </summary>
    public partial class UserControlDownloadImages : UserControl
    {
        private readonly IMediator _mediator;
        CancellationTokenSource cts = new CancellationTokenSource();
        public UserControlDownloadImages(IMediator mediator, Guid companyId)
        {
            _mediator = mediator;
            InitializeComponent();
            DownloadAsync(companyId);
                     
        }

        private async void DownloadAsync(Guid companyId)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            mainWindow.ShowLoadingIcon();
            mainWindow.HideMenu();

            var progress = new Progress<ProgressReportModel>();

            var result = await _mediator.Send(new GetAllByCompanyId.Query() { CompanyId = companyId });
            ActionText.Text = $"Pobieranie obrazów produktów firmy {result.Images[0].Product.Company.Name}";
            progress.ProgressChanged += ReportImagesProgress;
            var imgWatch = Stopwatch.StartNew();

            try
            {
                var images = await DownloadImagesAsync(result, progress,cts.Token);
                imgWatch.Stop();
                Result.Content = $"Pobieranie zakończono,{images.Count} obrazów zapisanych na dysk,czas wykonania: { imgWatch.Elapsed.TotalMinutes } minut.";
                ActionText.Text = "Zakończono.";
                mainWindow.HideLoadingIcon();
                mainWindow.ShowMenu();
            }
            catch (Exception)
            {
                ActionText.Text = "Proces anulowano";
                Result.Content = $"Zakończono,proces anulowany przez użytkownika.";
                DownloadProgress.Value = 100;
                mainWindow.HideLoadingIcon();
                mainWindow.ShowMenu();

            }

            CancelButton.Visibility = Visibility.Hidden;
            GoBackButton.Visibility = Visibility.Visible;
        }
        private void goBackButton_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault(mw => mw.IsActive);
            var companiesList = new UserControlCompaniesList(_mediator);
            mainWindow.GridMain.Children.Clear();
            mainWindow.GridMain.Children.Add(companiesList);
        }
        private void cancelOperation_Click(object sender, RoutedEventArgs e)
        {
            cts.Cancel();
        }
        private async Task<List<string>> DownloadImagesAsync(GetAllByCompanyId.Result result, IProgress<ProgressReportModel> progress, CancellationToken cancellationToken)
        {
            ProgressReportModel report = new ProgressReportModel();         
            var output = new List<string>();
          
            for (int i = 0; i < result.Images.Count; i++)
            {
                if (result.Images[i] != null)
                {
                    string url = await _mediator.Send(new DownloadById.Command() { Url = result.Images[i].Url, Path = result.Images[i].Path });
                    output.Add(url);
                    cancellationToken.ThrowIfCancellationRequested();
                    Result.Content = $"Pobieranie obrazu {url}";            
                    report.PercentageComplete = (output.Count * 100) / result.Images.Count;
                    progress.Report(report);

                }
            }
            return output;
        }        
        private void ReportImagesProgress(object sender, ProgressReportModel e)
        {
            DownloadProgress.Value = e.PercentageComplete;
        }
        private class ProgressReportModel
        {
            public int PercentageComplete { get; set; } = 0;
        }
    }
}
