﻿using FluentValidation;
using MediatR;
using System;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;


namespace WebScraper.App.Modules.Images
{
    public static class DownloadById
    {
        public class Command : IRequest<string>
        {
            public string Url { get; set; }
            public string Path { get; set; }
        }
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {

            }
        }

        public class Handler : IRequestHandler<Command, string>
        {           
            public async Task<string> Handle(Command command, CancellationToken token)
            {
                return await DownloadFileAsync(command.Url, command.Path);
            }
            private async Task<string> DownloadFileAsync(string url, string fileName, [Optional]CancellationToken token)
            {
                using (WebClient webClient = new WebClient())
                {
                    try
                    {
                        await webClient.DownloadFileTaskAsync(new Uri(url), fileName);
                        WaitForFile(fileName, token);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"{ex.Message} {Environment.NewLine} url:{url}");
                    }
                }
                return url;
            }
            private static bool IsFileReady(string filename)
            {
                try
                {
                    using (FileStream inputStream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                        return inputStream.Length > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            private static void WaitForFile(string filename, CancellationToken token)
            {
                while (!IsFileReady(filename))
                {
                    if (token.IsCancellationRequested)
                    {
                        return;
                    }
                }
            }
        }
       
    }
}


