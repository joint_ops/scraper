﻿namespace WebScraper.Core.Helpers
{
    public static class ExceptionMessageHelpers
    {
        public static string NotEmpty(string paramName)
            => $"{paramName} can not be empty.";
        public static string NoLongerThen(string paramName, int maxValue)
            => $"{paramName} can not be longer then {maxValue} characters.";
        public static string NoShorterThen(string paramName, int minValue)
           => $"{paramName} can not be shorter then {minValue} characters.";
        public static string NotZero(string paramName)
          => $"{paramName} can not have 0 value";
    }
}
