﻿using System;

namespace WebScraper.Core.Helpers
{
    public static class DateTimeHelper
    {
        public static DateTime Now
            => NowImpl();

        public static Func<DateTime> NowImpl =
            () => DateTime.UtcNow;
    }
}
