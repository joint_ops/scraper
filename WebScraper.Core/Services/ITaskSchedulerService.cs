﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WebScraper.Core.Services
{
    public interface ITaskSchedulerService :IService
    {
        Task StartEveryDayAt(DateTime dateTime);
    }
}
