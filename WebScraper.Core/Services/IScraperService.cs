﻿using System;
using System.Collections.Generic;

namespace WebScraper.Core.Services
{
    public interface IScraperService :IService
    {
        string GetCompanyName(string html);
        List<string> GetProductsUrls(string html);
        List<string> GetProductsNames(string html);
        string GetSingleProductName(string html);
        decimal GetPrice(string html);
        string GetCurrency(string html);
        List<string> GetImages(string html);
        string GetStockStatus(string html);
        string GetProductCode(string html);
        string GetUpcCode(string html);
        string GetQuantity(string html);      
        string GetDescription(string html);
        string GetSuplementTable(string html);
        DateTime? GetExpirationDate(string html);
        string GetShippingWeight(string html);
        string GetDimensions(string html);
        string Sanitize(string html);
        string GetUrl(string html);
        string Get1stCategory(string html);
        string Get2ndCategory(string html);
        string GetDescriptionList(string html);
        string GetSuggestedUse(string html);
        string GetIngredients(string html);
        public string GetBrand(string html);



    }
}
