﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace WebScraper.Core.Services
{
    public interface IFileService :IService
    {
        Task<string> DownloadFileAsync(string url, string fileName,[Optional]CancellationToken token);
        string ReadFile(string fileName);
    }
}
