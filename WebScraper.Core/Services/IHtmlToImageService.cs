﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebScraper.Core.Services
{
    public interface IHtmlToImageService :IService
    {
        void CreateImage(string html, string path);
    }
}
