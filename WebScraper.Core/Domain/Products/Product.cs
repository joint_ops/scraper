﻿using System;
using System.Collections.Generic;
using WebScraper.Core.Domain.Companies;
using WebScraper.Core.Exceptions;
using WebScraper.Core.Helpers;

namespace WebScraper.Core.Domain.Products
{
    public class Product
    {
        public Company Company { get; private set; }
        public List<ProductImage> ProductImages { get; private set; }

        public Guid Id { get; private set; }
        public Guid CompanyId { get; private set; }
        public string Url { get; private set; }
        public string Name { get; private set; }
        public string StockStatus { get; private set; }
        public decimal Price { get; private set; }
        public string Currency { get; private set; }
        public string Quantity { get; private set; }
        public string UpcCode { get; private set; }
        public string ProductCode { get; private set; }
        public string SupplementTable { get; private set; }       
        public string Description { get; private set; }
        public string DescriptionList { get; private set; }
        public string Ingredients { get; private set; }
        public string SuggestedUse { get; private set; }
        public string Dimensions { get; private set; }
        public string Weight { get; private set; }
        public string FirstCategory { get; private set; }
        public string SecondCategory { get; private set; }
        public string Brand { get; private set; }
        public DateTime? ExpirationDate { get; private set; }
        public bool IsComplete { get; private set; }
        public DateTime CreatedAt { get; private set; } = DateTimeHelper.Now;

        private Product() { }

        public Product(
            Guid id,
            Guid companyId,
            string name,
            string url,
            string stockStatus,
            decimal price,
            string currency,
            string quantity,
            string upcCode,
            string productCode,
            string suplementTable,
            string dimensions,
            string description,
            string descriptionList,
            string ingredients,
            string suggestedUse,
            string weight,
            string firstCategory,
            string secondCategory,
            string brand,
            DateTime? expirationDate,
            bool isComplete
            )
        {
            Id = id;
            CompanyId = companyId;
            SetName(name);
            SetUrl(url);
            SetStockStatus(stockStatus);
            SetPrice(price);
            SetCurrency(currency);
            SetQuantity(quantity);
            SetUpcCode(upcCode);
            SetProductCode(productCode);
            SetSuplementTable(suplementTable);
            SetDimensions(dimensions);
            SetDescription(description);
            SetDescriptionList(descriptionList);
            SetIngredients(ingredients);
            SetSuggestedUse(suggestedUse);
            SetWeigth(weight);
            SetFirstCategory(firstCategory);
            SetSecondCategory(secondCategory);
            SetBrand(brand);
            SetExpirationDate(expirationDate);
            SetIsComplete(isComplete);
        }
        
        public void SetName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new DomainException(DomainExceptionCodes.InvalidName,
                   ExceptionMessageHelpers.NotEmpty(nameof(Name)));

            if (name.Length > 255)
                throw new DomainException(DomainExceptionCodes.InvalidName,
                   ExceptionMessageHelpers.NoLongerThen(nameof(Name), 255));

            if (Name == name) return;
          
            Name = name;  
        }
        public void SetFirstCategory(string firstCategory)
        {
           
            if (FirstCategory == firstCategory) return;           
            FirstCategory = firstCategory;
        }
        public void SetSecondCategory(string secondCategory)
        {
            if (SecondCategory == secondCategory) return;          
            SecondCategory = secondCategory;
        }
        public void SetBrand(string brand)
        {
            if (Brand == brand) return;
            Brand = brand;
        }
        public void SetIngredients(string ingredients)
        {
            if (Ingredients == ingredients) return;        
            Ingredients = ingredients;
        }
        public void SetSuggestedUse(string suggestedUse)
        {
            if (SuggestedUse == suggestedUse) return;           
            SuggestedUse = suggestedUse;
        }
        public void SetDescriptionList(string descriptionList)
        {
            if (DescriptionList == descriptionList) return;            
            DescriptionList = descriptionList;
        }

        public void SetUrl(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
                throw new DomainException(DomainExceptionCodes.InvalidUrl,
                   ExceptionMessageHelpers.NotEmpty(nameof(Url)));

            if (url.Length < 10)
                throw new DomainException(DomainExceptionCodes.InvalidUrl,
                   ExceptionMessageHelpers.NoShorterThen(nameof(Url), 10));

            if (url.Length > 255)
                throw new DomainException(DomainExceptionCodes.InvalidUrl,
                   ExceptionMessageHelpers.NoLongerThen(nameof(Url), 255));

            if (Url == url) return;
           
            Url = url;
           
        }                 
        public void SetStockStatus(string stockStatus)
        {
            if (StockStatus == stockStatus) return;          
            StockStatus = stockStatus;        
        }
        public void SetIsComplete(bool param)
        {
            if (IsComplete == param) return;         
            IsComplete = param;
        }
        public void SetPrice(decimal price)
        {
            if (Price == price) return;            
            Price = price;
            
        }
        public void SetCurrency(string currency)
        {
            if (Currency == currency) return;          
            Currency = currency;           
        }
        public void SetDimensions(string dimensions)
        {
            if (Dimensions == dimensions) return;           
            Dimensions = dimensions;
        }
        public void SetWeigth(string weight)
        {
            if (Weight == weight) return;           
            Weight = weight;
        }
        public void SetExpirationDate(DateTime? expirationDate)
        {
            if (expirationDate == new DateTime?().GetValueOrDefault()) return;
            if (ExpirationDate == expirationDate) return;    
         
            ExpirationDate = expirationDate;
        }
        public void SetQuantity(string quantity)
        {
            if (Quantity == quantity) return;          
            Quantity = quantity;          
        }
        public void SetUpcCode(string upcCode)
        {
            if (UpcCode == upcCode) return;            
            UpcCode = upcCode;
        }
        public void SetProductCode(string productCode)
        {
            if (ProductCode == productCode) return;         
            ProductCode = productCode;
        }
        public void SetSuplementTable(string suplementTable)
        {
            if (SupplementTable == suplementTable) return;       
            SupplementTable = suplementTable;
        }
        public void SetDescription(string description)
        {
            if (Description == description) return;           
            Description = description;
        }
      

    }
}
