﻿using System;
using WebScraper.Core.Exceptions;
using WebScraper.Core.Helpers;

namespace WebScraper.Core.Domain.Products
{
    public class ProductImage
    {
        public Product Product { get; private set; }

        public Guid Id { get; private set; }
        public Guid ProductId { get; private set; }
        public string Url { get; private set; }
        public string Path { get; private set; }
        public DateTime CreatedAt { get; private set; } = DateTimeHelper.Now;
        public DateTime UpdatedAt { get; private set; }

        private ProductImage() { }

        public ProductImage(Guid productId,string url,string path)
        {
            ProductId = productId;
            SetUrl(url);
            SetPath(path);
        }
       
        public void SetUrl(string url)
        {
            if (Url == url) return;
            Url = url;
        }
        public void SetPath(string path)
        {
           
            if (Path == path) return;
            Path = path;
        }
        /*
        public void SetIsDownloaded()
        {
            if (IsDownloaded) return;
            IsDownloaded = true;
        }
        */
        public void SetUpdatedAt()
        {
            UpdatedAt = DateTimeHelper.Now;
        }


    }
}
