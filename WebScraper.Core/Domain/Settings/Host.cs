﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebScraper.Core.Domain.Settings
{
    public class Host
    {
        public Guid Id { get; private set; }
        public string Name { get;private set; }

        private Host()
        {

        }
        public Host(string name)
        {
            SetName(name);
        }
        public void SetName(string name)
        {
            if (Name == name) return;
            Name = name;
        }
       

    }
}
