﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebScraper.Core.Domain.Settings
{
    public class Country
    {
        public static Country PL => new Country("PL");
        public static Country US => new Country("US");
        public static Country DE => new Country("DE");

        public string Value { get; }

        public Country(string value)
        {
            Value = value;
        }
    }
}
