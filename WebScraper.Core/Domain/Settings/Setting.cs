﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebScraper.Core.Domain.Settings
{
    public class Setting
    {      
        public Guid Id { get; private set; }
        public string ImagesPath { get; set; }
        public string XlsPath { get; set; }
        public string Currency { get; set; }
        public string Language { get; set; }
        public string Country { get; set; }

        private Setting()
        {

        }
        public Setting(string imagesPath,string xlsPath,string country,string currency,string language)
        {
            SetImagesPath(imagesPath);
            SetXlsPath(xlsPath);
            SetCurrency(currency);
            SetCountry(country);
            SetLanguage(language);
        }
        public void SetImagesPath(string imagesPath)
        {
            if (ImagesPath == imagesPath) return;
            ImagesPath = imagesPath;
        }
        public void SetXlsPath(string xlsPath)
        {
            if (XlsPath == xlsPath) return;
            XlsPath = xlsPath;
        }
        public void SetCurrency(string currency)
        {
            if (Currency == currency) return;
            Currency = currency;
        }
        public void SetLanguage(string language)
        {
            if (Language == language) return;
            Language = language;
        }
        public void SetCountry(string country)
        {
            if (Country == country) return;
            Country = country;
        }

    }
}
