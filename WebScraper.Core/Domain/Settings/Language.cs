﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebScraper.Core.Domain.Settings
{
    public class Language
    {
        public static Language English => new Language("en-US");
        public static Language German => new Language("de-DE");

        public string Value { get; }

        public Language(string value)
        {
            Value = value;
        }
    }
}
