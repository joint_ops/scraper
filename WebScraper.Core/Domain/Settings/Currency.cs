﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebScraper.Core.Domain.Settings
{
    public class Currency
    {
        public static Currency PLN => new Currency("PLN");
        public static Currency USD => new Currency("USD");
        public static Currency EUR => new Currency("EUR");

        public string Value { get; }
        private Currency()
        {

        }

        public Currency(string value)
        {
            Value = value;
        }
       
    }
}
