﻿using System;
using System.Collections.Generic;
using System.Text;
using WebScraper.Core.Helpers;

namespace WebScraper.Core.Domain.Companies
{
    public class Page
    {
        public Company Company { get; private set; }
        public Guid Id { get; private set; }
        public Guid CompanyId { get; private set; }
        public string Url { get; private set; }
        public string Html { get; private set; }

        private Page()
        {

        }
        public Page(Guid companyId,string html,string url)
        {
            CompanyId = companyId;
            SetHtml(html);
            SetUrl(url);
        }        
        public void SetHtml(string html)
        {         
            if (Html == html) return;           
            Html = html;
        }
        
        public void SetUrl(string url)
        {
            if (Url == url) return;          
            Url = url;
        }
       

    }
}
