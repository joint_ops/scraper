﻿using System;
using System.Collections.Generic;
using WebScraper.Core.Domain.Products;
using WebScraper.Core.Exceptions;
using WebScraper.Core.Helpers;

namespace WebScraper.Core.Domain.Companies
{
    public class Company
    {
        public List<Product> Products { get; private set; }
        public List<Page> Pages { get; private set; }

        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public DateTime CreatedAt { get; private set; } = DateTimeHelper.Now;
        public DateTime? UpdatedAt { get; private set; }
        public DateTime? CheckedAt { get; private set; }
        public int UpdatedTimes { get; private set; }
        public bool HasCompletedProducts { get; private set; }
        public bool HasCompletedImages { get; private set; }
        public bool CanBeUpdated { get; private set; }

        private Company() { }

        public Company(Guid id,string name)
        {
            Id = id;
            SetName(name);
        }              
        public void SetName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new DomainException(DomainExceptionCodes.InvalidName,
                   ExceptionMessageHelpers.NotEmpty(nameof(Name)));

            if (name.Length > 255)
                throw new DomainException(DomainExceptionCodes.InvalidName,
                   ExceptionMessageHelpers.NoLongerThen(nameof(Name), 255));

            if (Name == name) return;

            if (Name != null)
            {
                SetUpdatedAt();
            }
            Name = name;
        }
      
        private void SetUpdatedAt()
        {
            UpdatedAt = DateTimeHelper.Now;
            UpdatedTimes += 1;
        }
        public void SetCheckedAt()
        {
            CheckedAt = DateTimeHelper.Now;
        }
        
        public void SetHasCompletedProducts(bool param)
        {
            if (HasCompletedProducts == param) return;
            HasCompletedProducts = param;
        }
        public void SetHasCompletedImages(bool param)
        {
            if (HasCompletedImages == param) return;
            HasCompletedImages = param;
        }
        public void SetCanBeUpdated(bool param)
        {
            if (CanBeUpdated == param) return;
            CanBeUpdated = param;
        }

    }
}
