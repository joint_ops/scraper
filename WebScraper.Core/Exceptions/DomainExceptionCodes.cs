﻿namespace WebScraper.Core.Exceptions
{
    public static class DomainExceptionCodes
    {
        public static string InvalidName => "invalid name";
        public static string InvalidUrl => "invalid url";
        public static string InvalidPath => "invalid path";
        public static string InvalidPrice => "invalid price";
        public static string InvalidDescription => "invalid description";
        public static string InvalidSuplementTable => "invalid suplement table";
        public static string InvalidQuantity => "invalid quantity";
        public static string InvalidUpcCode => "invalid upc code";
        public static string InvalidProductCode => "invalid product code";
    }
}

